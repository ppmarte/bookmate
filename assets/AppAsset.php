<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * Main application asset bundle.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/site.css',
    ];
    public $js = [
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap4\BootstrapAsset',
    ];
}

class FontAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        '//fonts.googleapis.com/css?family=Open+Sans:400,700',
        '//fonts.googleapis.com/css?family=Playfair+Display:400,700i,900',
        '//fonts.googleapis.com/css?family=Oswald:400,700',
          '//fonts.googleapis.com/css?family=Montserrat:400,500,700,900',
         '//fonts.googleapis.com/css?family=Poppins:400,500,700,900',
       
    ];
    public $cssOptions = [
        'type' => 'text/css',
    ];
}