<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "comentarios".
 *
 * @property int $id
 * @property int|null $cod_agregar
 * @property string|null $comentario
 *
 * @property Agregacion $codAgregar
 */
class Comentarios extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'comentarios';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['cod_agregar'], 'integer'],
            [['comentario'], 'string'],
            [['cod_agregar'], 'exist', 'skipOnError' => true, 'targetClass' => Agregacion::class, 'targetAttribute' => ['cod_agregar' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'cod_agregar' => 'Código de Agregación',
            'comentario' => 'Comentario',
        ];
    }

    /**
     * Gets query for [[CodAgregar]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodAgregar()
    {
        return $this->hasOne(Agregacion::class, ['id' => 'cod_agregar']);
    }
}
