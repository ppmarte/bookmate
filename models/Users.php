<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "users".
 *
 * @property int $id
 * @property string|null $username
 * @property string|null $email
 * @property string|null $password
 * @property string $authKey
 * @property string $accessToken
 * @property int $activate
 * @property string|null $f_registro
 * @property string|null $biografia
 *
 * @property Estanterias[] $codEstanterias
 * @property Libros[] $codLibros
 * @property Retos[] $codRetos
 * @property Gestiones[] $gestiones
 * @property Leen[] $leens
 * @property Participaciones[] $participaciones
 */
class Users extends \yii\db\ActiveRecord {

    public $password_repeat;

    public static function getDb() {
        return Yii::$app->db;
    }

    /**
     * {@inheritdoc}
     */
    public static function tableName() {
        return 'users';
    }

    public $file;

    /**
     * {@inheritdoc}
     */
    public function rules() {
        return [
            [['authKey', 'accessToken', 'email', 'username', 'password'], 'required', 'message' => 'Campo requerido'],
            [['activate'], 'integer'],
            [['f_registro'], 'safe'],
            [['f_registro'], 'default', 'value' => date("Y-m-d")],
            [['file'], 'file', 'extensions' => 'jpg', 'maxFiles' => '1'],
            [['username', 'email'], 'string', 'max' => 100],
            [['username'], 'match', 'pattern' => "/^[0-9a-záéíóúÁÉÍÓÚüÜàèìòùÀÈÌÒÙ\s]+$/i", 'message' => 'Sólo se aceptan letras y números'],
            [['email'], 'email', 'message' => 'Formanto no valido'],
            [['password', 'accessToken'], 'string', 'max' => 50],
            [['password'], 'match', 'pattern' => "/^.{6,50}$/", 'message' => 'Mínimo 6 y máximo 50 caracteres'],
            [['password_repeat'], 'compare', 'compareAttribute' => 'password', 'message' => 'Las contraseñas no coinciden'],
            [['authKey'], 'string', 'max' => 250],
            [['biografia'], 'string', 'max' => 2500],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'username' => 'Nombre de usuario',
            'email' => 'Email',
            'password' => 'Contraseña',
            'authKey' => 'Auth Key',
            'accessToken' => 'Access Token',
            'activate' => 'Activo',
            'f_registro' => 'Fecha de Registro',
            'biografia' => 'Biografia',
        ];
    }
    
//Metodo para formatear la fecha
    public function beforeSave($insert) {
        if ($this->f_registro) {
            $this->f_registro = date("Y-m-d", strtotime($this->f_registro));
        }


        return parent::beforeSave($insert);
    }
//Metodo para formatear la fecha
    public function afterFind() {
        if ($this->f_registro) {
            $this->f_registro = date("d-m-Y", strtotime($this->f_registro));
        }
    }

    /**
     * Gets query for [[CodEstanterias]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodEstanterias() {
        return $this->hasMany(Estanterias::class, ['id' => 'cod_estanteria'])->viaTable('gestiones', ['cod_user' => 'id']);
    }

    /**
     * Gets query for [[CodLibros]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodLibros() {
        return $this->hasMany(Libros::class, ['id' => 'cod_libro'])->viaTable('leen', ['cod_user' => 'id']);
    }

    /**
     * Gets query for [[CodRetos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodRetos() {
        return $this->hasMany(Retos::class, ['id' => 'cod_reto'])->viaTable('participaciones', ['cod_user' => 'id']);
    }

    /**
     * Gets query for [[Gestiones]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getGestiones() {
        return $this->hasMany(Gestiones::class, ['cod_user' => 'id']);
    }

    /**
     * Gets query for [[Leens]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getLeens() {
        return $this->hasMany(Leen::class, ['cod_user' => 'id']);
    }

    /**
     * Gets query for [[Participaciones]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getParticipaciones() {
        return $this->hasMany(Participaciones::class, ['cod_user' => 'id']);
    }

}
