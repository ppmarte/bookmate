<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tags".
 *
 * @property int $id
 * @property int|null $cod_agregar
 * @property string|null $tag
 *
 * @property Agregacion $codAgregar
 */
class Tags extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tags';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['cod_agregar'], 'integer'],
            [['tag'], 'string', 'max' => 50],
            [['cod_agregar', 'tag'], 'unique', 'targetAttribute' => ['cod_agregar', 'tag']],
            [['cod_agregar'], 'exist', 'skipOnError' => true, 'targetClass' => Agregacion::class, 'targetAttribute' => ['cod_agregar' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'cod_agregar' => 'Código agregación',
            'tag' => 'Tag',
        ];
    }

    /**
     * Gets query for [[CodAgregar]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodAgregar()
    {
        return $this->hasOne(Agregacion::class, ['id' => 'cod_agregar']);
    }
}
