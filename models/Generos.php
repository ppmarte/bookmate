<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "generos".
 *
 * @property int $id
 * @property int|null $cod_libro
 * @property string|null $genero
 *
 * @property Libros $codLibro
 */
class Generos extends \yii\db\ActiveRecord {

    /**
     * {@inheritdoc}
     */
    public static function tableName() {
        return 'generos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules() {
        return [
            [['cod_libro'], 'integer'],
            [['genero'], 'string', 'max' => 50],
            [['cod_libro', 'genero'], 'unique', 'targetAttribute' => ['cod_libro', 'genero']],
            [['cod_libro'], 'exist', 'skipOnError' => true, 'targetClass' => Libros::class, 'targetAttribute' => ['cod_libro' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'cod_libro' => 'Código del libro',
            'genero' => 'Género',
        ];
    }

    /**
     * Gets query for [[CodLibro]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodLibro() {
        return $this->hasOne(Libros::class, ['id' => 'cod_libro']);
    }

}
