<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "retos".
 *
 * @property int $id
 * @property string|null $nombre
 * @property string|null $f_inicio
 * @property string|null $f_fin
 * @property int $completado
 *
 * @property Users[] $codUsers
 * @property Participaciones[] $participaciones
 */
class Retos extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'retos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['f_inicio', 'f_fin'], 'safe'],
    
            [['nombre'], 'string', 'max' => 50],
             [['nombre'],'match', 'pattern' =>"/^([0-9a-z\sáéíóúñÁÉÍÓÚüÜàèìòùÀÈÌÒÙÑ.]+)$/i", 'message' => 'Sólo se aceptan letras y números'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nombre' => 'Nombre',
            'f_inicio' => 'Inicio',
            'f_fin' => 'Fin',

        ];
    }
    
        /*metodo para guardar las fechas en el formato correcto*/
     public function beforeSave($insert)
        {
                if ($this->f_inicio) {
                    $this->f_inicio = date("Y-m-d", strtotime($this->f_inicio));
                }
                 if ($this->f_fin) {
                    $this->f_fin = date("Y-m-d", strtotime($this->f_fin));
                }
                return parent::beforeSave($insert);
        }
        
        /*Metodo para mostrar la fecha en un formato apto para el usuario*/
        public function afterFind()
        {
                if ($this->f_inicio)
                {
                    $this->f_inicio = date("d-m-Y", strtotime($this->f_inicio));
                }

                 if ($this->f_fin)
                {
                    $this->f_fin = date("d-m-Y", strtotime($this->f_fin));
                }
        }


    /**
     * Gets query for [[CodUsers]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodUsers()
    {
        return $this->hasMany(Users::class, ['id' => 'cod_user'])->viaTable('participaciones', ['cod_reto' => 'id']);
    }

    /**
     * Gets query for [[Participaciones]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getParticipaciones()
    {
        return $this->hasMany(Participaciones::class, ['cod_reto' => 'id']);
    }
}
