<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "participaciones".
 *
 * @property int $id
 * @property int|null $cod_user
 * @property int|null $cod_reto
 * @property int|null $objetivo
 *
 * @property Retos $codReto
 * @property Users $codUser
 */
class Participaciones extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'participaciones';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['cod_user', 'cod_reto', 'objetivo'], 'integer'],
            [['objetivo'], 'integer', 'min' => 1],
             [['objetivo'],'match', 'pattern' => "/^([0-9]+)$/i", 'message' => 'Sólo se aceptan números'],
            [['cod_user', 'cod_reto'], 'unique', 'targetAttribute' => ['cod_user', 'cod_reto']],
            [['cod_reto'], 'exist', 'skipOnError' => true, 'targetClass' => Retos::class, 'targetAttribute' => ['cod_reto' => 'id']],
            [['cod_user'], 'exist', 'skipOnError' => true, 'targetClass' => Users::class, 'targetAttribute' => ['cod_user' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'cod_user' => 'Código del usuario',
            'cod_reto' => 'Código del reto',
            'objetivo' => 'Objetivo',
  
        ];
    }

    /**
     * Gets query for [[CodReto]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodReto()
    {
        return $this->hasOne(Retos::class, ['id' => 'cod_reto']);
    }

    /**
     * Gets query for [[CodUser]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodUser()
    {
        return $this->hasOne(Users::class, ['id' => 'cod_user']);
    }
}
