<?php

namespace app\models;

use yii\db\Expression;
use Yii;

/**
 * This is the model class for table "libros".
 *
 * @property int $id
 * @property string|null $ISBN
 * @property string|null $titulo
 * @property string|null $sipnosis
 * @property int|null $num_pag
 * @property string|null $f_publi
 * @property int|null $pagleidas
 * @property float|null $calificacion
 *
 * @property Agregacion[] $agregacions
 * @property Escritores[] $codEscritors
 * @property Escriben[] $escribens
 * @property Generos[] $generos
 */
class Libros extends \yii\db\ActiveRecord {

    public $cod_escritor;

    /**
     * {@inheritdoc}
     */
    public static function tableName() {
        return 'libros';
    }

    /**
     * {@inheritdoc}
     */
    public function rules() {
        return [
            [['sipnosis'], 'string'],
            [['num_pag'], 'integer'],
            [['f_publi'], 'safe'],
            [['calificacion'], 'number'],
            [['ISBN'], 'string', 'max' => 20],
            [['titulo'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'ISBN' => 'ISBN',
            'titulo' => 'Título',
            'sipnosis' => 'Sipnosis',
            'num_pag' => 'Páginas',
            'f_publi' => 'Fecha de publicación',
            'calificacion' => 'Calificación',
        ];
    }

    /**
     * Gets query for [[Agregacions]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getAgregacions() {
        return $this->hasMany(Agregacion::class, ['cod_libro' => 'id']);
    }

    /**
     * Gets query for [[CodEscritors]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodEscritors() {
        return $this->hasMany(Escritores::class, ['id' => 'cod_escritor'])->viaTable('escriben', ['cod_libro' => 'id']);
    }

    /**
     * Gets query for [[Escribens]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getEscribens() {
        return $this->hasMany(Escriben::class, ['cod_libro' => 'id']);
    }

    /**
     * Gets query for [[Generos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getGeneros() {
        return $this->hasMany(Generos::class, ['cod_libro' => 'id']);
    }

    //Método que devuelve la calificación de un  usuario y la muestra en el StarRating de la vista
    public function getCalificacionUsuario() {
        $agregacionUsuario = Agregacion::findOne(['cod_libro' => $this->id, 'cod_user' => Yii::$app->user->identity->id]);

        if ($agregacionUsuario) {
            return $agregacionUsuario->calificacion;
        }

        return 0; // Otra valor por defecto si no se encuentra la calificación del usuario
    }

    //Método que devuelve la calificación media de un libroy la muestra en el StarRating de la vista
    public function getCalificacionMedia() {

        return Agregacion::find()
                        ->select(new Expression('AVG(calificacion)'))
                        ->where(['cod_libro' => $this->id])
                        ->andWhere(['>', 'calificacion', 0])
                        ->scalar();
    }

    //Método que devuelve el número de calificación que tiene un libro y la muestra en el StarRating de la vista
    public function getNumCalificaciones() {
        return Agregacion::find()
                        ->where(['cod_libro' => $this->id])
                        ->andWhere(['>', 'calificacion', 0])
                        ->count();
    }

    public function beforeSave($insert) {
        if ($this->f_publi) {
            $this->f_publi = date("Y-m-d", strtotime($this->f_publi));
        }


        return parent::beforeSave($insert);
    }

    public function afterFind() {
        if ($this->f_publi) {
            $this->f_publi = date("d-m-Y", strtotime($this->f_publi));
        }
    }

    //Método para calcular el % léido de un libro
    public function porcentaje_leido($pagleidas) {
        return $pagleidas / $this->num_pag * 100;
    }

}
