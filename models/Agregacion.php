<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "agregacion".
 *
 * @property int $id
 * @property int $cod_libro
 * @property int $cod_user
 * @property int $cod_estanterias
 * @property string $f_agregado
 * @property float $calificacion
 * @property int $paginasleidas
 *
 * @property Estanterias $codEstanterias
 * @property Libros $codLibro
 * @property Users $codUser
 * @property Comentarios[] $comentarios
 * @property Tags[] $tags
 */
class Agregacion extends \yii\db\ActiveRecord {

    /**
     * {@inheritdoc}
     */
    public static function tableName() {
        return 'agregacion';
    }

    /**
     * {@inheritdoc}
     */
    public function rules() {
        return [
            [['cod_libro', 'cod_user', 'cod_estanterias'], 'required'],
            [['cod_libro', 'cod_user', 'cod_estanterias', 'paginasleidas'], 'integer'],
            [['f_agregado'], 'safe'],
            [['calificacion'], 'number'],
            [['cod_libro', 'cod_user'], 'unique', 'targetAttribute' => ['cod_libro', 'cod_user']],
            [['cod_estanterias'], 'exist', 'skipOnError' => true, 'targetClass' => Estanterias::class, 'targetAttribute' => ['cod_estanterias' => 'id']],
            [['cod_libro'], 'exist', 'skipOnError' => true, 'targetClass' => Libros::class, 'targetAttribute' => ['cod_libro' => 'id']],
            [['cod_user'], 'exist', 'skipOnError' => true, 'targetClass' => Users::class, 'targetAttribute' => ['cod_user' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'cod_libro' => 'Código del libro',
            'cod_user' => 'Código del usuario',
            'cod_estanterias' => 'Código de la estantería',
            'f_agregado' => 'Fecha',
            'calificacion' => 'Calificación',
            'paginasleidas' => 'Páginas leídas',
        ];
    }

    /**
     * Gets query for [[CodEstanterias]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodEstanterias() {
        return $this->hasOne(Estanterias::class, ['id' => 'cod_estanterias']);
    }

    /**
     * Gets query for [[CodLibro]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodLibro() {
        return $this->hasOne(Libros::class, ['id' => 'cod_libro']);
    }

    /**
     * Gets query for [[CodUser]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodUser() {
        return $this->hasOne(Users::class, ['id' => 'cod_user']);
    }

    /**
     * Gets query for [[Comentarios]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getComentarios() {
        return $this->hasMany(Comentarios::class, ['cod_agregar' => 'id']);
    }

    /**
     * Gets query for [[Tags]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getTags() {
        return $this->hasMany(Tags::class, ['cod_agregar' => 'id']);
    }

    
    //Metodos para formatear la fecha
    public function beforeSave($insert) {
        if ($this->f_agregado) {
            $this->f_agregado = date("Y-m-d", strtotime($this->f_agregado));
        }


        return parent::beforeSave($insert);
    }

    public function afterFind() {
        if ($this->f_agregado) {
            $this->f_agregado = date("d-m-Y", strtotime($this->f_agregado));
        }
    }

}
