<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "escritores".
 *
 * @property int $id
 * @property string|null $nombre
 * @property string|null $f_nacimiento
 * @property string|null $nacionalidad
 * @property string|null $biografia
 *
 * @property Libros[] $codLibros
 * @property Escriben[] $escribens
 */
class Escritores extends \yii\db\ActiveRecord {

    /**
     * {@inheritdoc}
     */
    public static function tableName() {
        return 'escritores';
    }

    /**
     * {@inheritdoc}
     */
    public function rules() {
        return [
            [['f_nacimiento'], 'safe'],
            [['nombre', 'nacionalidad'], 'string', 'max' => 50],
            [['biografia'], 'string', 'max' => 2500],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'nombre' => 'Nombre',
            'f_nacimiento' => 'Fecha de Nacimiento',
            'nacionalidad' => 'Nacionalidad',
            'biografia' => 'Biografia',
        ];
    }

    //Metodo para formatear la fecha
    public function beforeSave($insert) {
        if ($this->f_nacimiento) {
            $this->f_nacimiento = date("Y-m-d", strtotime($this->f_nacimiento));
        }


        return parent::beforeSave($insert);
    }

    public function afterFind() {
        if ($this->f_nacimiento) {
            $this->f_nacimiento = date("d-m-Y", strtotime($this->f_nacimiento));
        }
    }

    /**
     * Gets query for [[CodLibros]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodLibros() {
        return $this->hasMany(Libros::class, ['id' => 'cod_libro'])->viaTable('escriben', ['cod_escritor' => 'id']);
    }

    /**
     * Gets query for [[Escribens]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getEscribens() {
        return $this->hasMany(Escriben::class, ['cod_escritor' => 'id']);
    }

    public static function getEscritoresOptions() {
        $escritores = self::find()->orderBy('nombre')->all();
        return \yii\helpers\ArrayHelper::map($escritores, 'id', 'nombre');
    }

}
