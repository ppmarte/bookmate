<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "escriben".
 *
 * @property int $id
 * @property int|null $cod_escritor
 * @property int|null $cod_libro
 *
 * @property Escritores $codEscritor
 * @property Libros $codLibro
 */
class Escriben extends \yii\db\ActiveRecord {

    /**
     * {@inheritdoc}
     */
    public static function tableName() {
        return 'escriben';
    }

    /**
     * {@inheritdoc}
     */
    public function rules() {
        return [
            [['cod_escritor', 'cod_libro'], 'integer'],
            [['cod_escritor', 'cod_libro'], 'unique', 'targetAttribute' => ['cod_escritor', 'cod_libro']],
            [['cod_escritor'], 'exist', 'skipOnError' => true, 'targetClass' => Escritores::class, 'targetAttribute' => ['cod_escritor' => 'id']],
            [['cod_libro'], 'exist', 'skipOnError' => true, 'targetClass' => Libros::class, 'targetAttribute' => ['cod_libro' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'cod_escritor' => 'Código del escritor',
            'cod_libro' => 'Código del libro',
        ];
    }

    /**
     * Gets query for [[CodEscritor]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodEscritor() {
        return $this->hasOne(Escritores::class, ['id' => 'cod_escritor']);
    }

    /**
     * Gets query for [[CodLibro]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodLibro() {
        return $this->hasOne(Libros::class, ['id' => 'cod_libro']);
    }

}
