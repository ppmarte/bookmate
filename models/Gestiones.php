<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "gestiones".
 *
 * @property int $id
 * @property int|null $cod_user
 * @property int|null $cod_estanteria
 *
 * @property Estanterias $codEstanteria
 * @property Users $codUser
 */
class Gestiones extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'gestiones';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['cod_user', 'cod_estanteria'], 'integer'],
            [['cod_estanteria', 'cod_user'], 'unique', 'targetAttribute' => ['cod_estanteria', 'cod_user']],
            [['cod_estanteria'], 'exist', 'skipOnError' => true, 'targetClass' => Estanterias::class, 'targetAttribute' => ['cod_estanteria' => 'id']],
            [['cod_user'], 'exist', 'skipOnError' => true, 'targetClass' => Users::class, 'targetAttribute' => ['cod_user' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'cod_user' => 'Código del usuario',
            'cod_estanteria' => 'Código de la estantería',
        ];
    }

    /**
     * Gets query for [[CodEstanteria]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodEstanteria()
    {
        return $this->hasOne(Estanterias::class, ['id' => 'cod_estanteria']);
    }

    /**
     * Gets query for [[CodUser]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodUser()
    {
        return $this->hasOne(Users::class, ['id' => 'cod_user']);
    }
}
