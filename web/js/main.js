
var baseUrl;
var id = null;
// Funciones para cambiar el tema
const temaOscuro = () => {
  document.body.classList.add('dark');
  document.querySelector("#dl-icon").setAttribute("class", "fas fa-moon");
};

const temaClaro = () => {
  document.body.classList.remove('dark');
  document.querySelector("#dl-icon").setAttribute("class", "fas fa-sun");
};

// Función para cambiar el tema y guardar la elección en el almacenamiento local
const cambiarTema = () => {
  document.body.classList.contains("dark") ? temaClaro() : temaOscuro();
  localStorage.setItem('modo', document.body.classList.contains('dark') ? 'dark' : 'light');
};

// Comprueba si hay una elección del modo guardada en el almacenamiento local
const modo = localStorage.getItem('modo');
if (modo === 'dark') {
  temaOscuro();
} else if (modo === 'light') {
  temaClaro();
}



//Función para buscar
function buscar_ahora() {
  var busqueda = document.getElementById("buscar").value.trim(); // Obtener la búsqueda y eliminar los espacios en blanco al inicio y al final
  if (busqueda.length > 0) { // Verificar que la búsqueda no esté vacía
    window.location.href = baseUrl + '/site/buscar?q=' + encodeURIComponent(busqueda); // Codificar la búsqueda antes de enviarla al controlador
  }
}

//Función para buscar en el momento
function buscar_en_tiempo_real() {
  var busqueda = document.getElementById("buscar").value;
  var texto_busqueda = document.getElementById("buscar").value;
  var resultados = document.getElementById("resultados-busqueda");
  
  if (busqueda.length >= 3) {
  $.ajax({
url:baseUrl + '/site/busquedaajax?q=' + busqueda,
    type: 'GET',
    data: {
      'q': busqueda
    },
success: function(response) {
  var registros = response.registros;
  $('#resultados-busqueda').empty();
  if (registros.length === 0) {
    $('#resultados-busqueda').text('No se encontraron resultados');
  } else {
    for (var i = 0; i < registros.length; i++) {
      var registro = registros[i];
      var escritores = registro.escritores;
      var div = $('<div>');
       //var enlace = $('<a>').attr('href', '' + registro.id).text(registro.titulo);
      var link = $('<a>').attr('href', baseUrl+'/libros/view?id=' + registro.id).text(registro.titulo);
      div.append(link);
      if (escritores && escritores.length > 0) {
        div.append(' - ' + escritores[0].nombre);
      }
  
     $('#resultados-busqueda').append(div);
          }
        }
      }
    });

    // Agregar evento 'click' al documento para cerrar la zona de resultados de búsqueda cuando se hace clic fuera de ella
    document.addEventListener('click', function(event) {
      if (event.target != resultados && event.target != document.getElementById("buscar")) {
        resultados.style.display = 'none';
        
      }
    });
  } else {
      resultados.innerHTML = "  Buscando...";
      resultados.style.padding = '5px';
      
  }
  
  // Agregar evento 'click' al cuadro de búsqueda para mostrar la zona de resultados
  document.getElementById("buscar").addEventListener('click', function(event) {
    if (resultados.style.display === 'none' && document.getElementById("buscar").value.length >= 3) {
      resultados.style.display = 'block';
    }
  });
}


function limpiar_resultados() {
  document.getElementById("resultados-busqueda").innerHTML = "";
}





//ACTUALIZA EL TEXTO EN LA PAG. ESTANTERIA
function actualizarTextoProgreso(paginasLeidas, numPaginas, idLibro) {
  var porcentajeLeido = Math.round(paginasLeidas / numPaginas * 100);
    var textoProgreso = paginasLeidas + ' / ' + numPaginas + '(' + porcentajeLeido + '%)';
    document.getElementById('texto-progreso'+ idLibro).textContent = textoProgreso;
}


function actualizarProgreso() {
    var progreso = obtenerProgreso();
    var datos = JSON.parse(progreso);
    var porcentaje = datos.paginasleidas / window.numPaginas * 100;
    var progresoBarra = document.querySelector('.progress-bar');
    progresoBarra.style.width = porcentaje + '%';
    progresoBarra.setAttribute('aria-valuenow', datos.paginasleidas);
    progresoBarra.setAttribute('aria-valuemax', window.numPaginas);
    console.log('stoy aqu1');
    actualizarTextoProgreso(response.paginasleidas, response.num_pag, idLibro);// Llama a la función para actualizar el texto
    console.log('stoy aq2');
}

document.addEventListener("DOMContentLoaded", function() {
  const btnsActualizar = document.querySelectorAll('.btn-actualizar');
  btnsActualizar.forEach(btn => {
    btn.addEventListener('click', function(event) {
      event.preventDefault();
      const idLibro = this.dataset.id;
       console.log('tengo el id');
        console.log(idLibro);
     actualizarPaginasLeidas(idLibro); // Obtener el valor del atributo "data-id" del botón
    });
  });
});




function actualizarPaginasLeidas(idLibro) {
  var numPaginas = document.querySelector('.btn-actualizar[data-id="' + idLibro + '"]').dataset.paginas;
  var paginasLeidas = parseInt(document.querySelector('#paginasLeidas' + idLibro).value);


console.log(idLibro);
console.log(typeof(paginasLeidas));

  if (paginasLeidas < 0 || paginasLeidas > numPaginas) {
    alert('El número de páginas leídas debe estar entre 0 y ' + numPaginas + '.');
    return;
  }

  var data = { paginasLeidas };
  
  
var csrfToken = $('meta[name="csrf-token"]').attr("content");
  $.ajax({
    url:  baseUrl + '/agregacion/actualizarpaginasleidas?id=' + idLibro,
     type: 'POST',
     data: data,
     headers: {'X-CSRF-Token': csrfToken},
     success: function(response){
   
      if (response.success) {
          
             console.log(response.porcentaje_leido);
        // Actualizar el valor del número de páginas leídas en la página
        $('#paginasLeidas' + idLibro).val(response.paginasleidas);

        // Actualizar el ancho y el texto de la barra de progreso
        var porcentajeLeido = response.porcentaje_leido;
        var $progreso = $('#progresoLibro' + idLibro + ' .progress-bar');
        $progreso.css('width', porcentajeLeido + '%');
        $progreso.attr('aria-valuenow', response.paginasleidas);
        //$progreso.html(response.paginas_leidas + ' / ' + response.num_pag + ' pág. (' + porcentajeLeido + '%)');
        actualizarTextoProgreso(response.paginasleidas, response.num_pag, idLibro);

        // Actualizar el atributo paginas_leidas en el modelo libro
        $progreso.closest('.libro').attr('data-paginasleidas', response.paginasleidas);
      } else {
        alert('ERROR.');
      }
    },
    error: function() {
      alert('Hay un error al actualizar las páginas leídas.');
    }
  });
}


//CAMBIO DEL NAVBAR CON EL SCROLL
$(document).ready(function() {
    $(window).scroll(function() {
        // Altura de desplazamiento en la que el navbar cambia de color
        var scrollHeight = 350; // Cambia esto al valor deseado

        if ($(window).scrollTop() >= scrollHeight) {
            // Si el desplazamiento es igual o mayor que la altura de desplazamiento, añade la clase 'navbar-scrolled'
            $('.bg-dark').addClass('bg-dark-scrolled');
        } else {
            // Si el desplazamiento es menor que la altura de desplazamiento, elimina la clase 'navbar-scrolled'
            $('.bg-dark').removeClass('bg-dark-scrolled');
        }
    });
});




