<?php

namespace app\controllers;

use app\models\Estanterias;
use app\models\Agregacion;
use app\models\Tags;
use app\models\Libros;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\BadRequestHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;
use yii;

/**
 * AgregacionController implements the CRUD actions for Agregacion model.
 */
class AgregacionController extends Controller {

    /**
     * @inheritDoc
     */
    public function behaviors() {
        return array_merge(
                parent::behaviors(),
                [
                    'verbs' => [
                        'class' => VerbFilter::className(),
                        'actions' => [
                            'delete' => ['POST'],
                        ],
                    ],
                ]
        );
    }

    /**
     * Lists all Agregacion models.
     *
     * @return string
     */
    public function actionIndex() {
        $dataProvider = new ActiveDataProvider([
            'query' => Agregacion::find(),
                /*
                  'pagination' => [
                  'pageSize' => 50
                  ],
                  'sort' => [
                  'defaultOrder' => [
                  'id' => SORT_DESC,
                  ]
                  ],
                 */
        ]);

        return $this->render('index', [
                    'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Agregacion model.
     * @param int $id ID
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id) {
        return $this->render('view', [
                    'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Agregacion model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate() {
        $model = new Agregacion();

        if ($this->request->isPost) {
            if ($model->load($this->request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
                    'model' => $model,
        ]);
    }

    /**
     * Updates an existing Agregacion model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $id ID
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id) {
        $model = $this->findModel($id);

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
                    'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Agregacion model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $id ID
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id) {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Agregacion model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $id ID
     * @return Agregacion the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = Agregacion::findOne(['id' => $id])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    //Metodo para añadir un libro a una estantería y ponerle un tag
    public function actionAddToShelf($id) {
        // Obtener el modelo de libro
        $libro = Libros::findOne($id);

        // Crear un nuevo modelo Agregacion y asignar los valores correspondientes
        $agregacion = new Agregacion();
        $agregacion->cod_user = Yii::$app->user->identity->id;
        $agregacion->cod_libro = $libro->id;
        $agregacion->f_agregado = date("Y-m-d");

        // Obtener todas las estanterías
        $estanterias = Estanterias::find()->all();

        $tags = new Tags();

        if ($agregacion->load(Yii::$app->request->post())) {
            // Buscar una entrada existente en la tabla de agregación
            $entry = Agregacion::find()->where([
                        'cod_user' => $agregacion->cod_user,
                        'cod_libro' => $agregacion->cod_libro,
                    ])->one();

            if ($entry !== null) {
                // Si se encontró una entrada, actualizar el código de estantería
                $entry->cod_estanterias = $agregacion->cod_estanterias;
                $entry->f_agregado = date("Y-m-d");
                $entry->save(false);

                $agregacionId = $entry->id; // Obtener el ID de Agregacion después de guardarlo
            } else {
                // Si no se encontró una entrada, guardar un nuevo modelo de Agregacion
                $agregacion->cod_estanterias = $agregacion->cod_estanterias;
                $agregacion->save(false);
                $agregacionId = $agregacion->id; // Obtener el ID de Agregacion después de guardarlo
            }

            $tagsInput = Yii::$app->request->post('Tags')['tag'];
            // Asignar el ID del modelo Agregacion al modelo Tags
            if (!empty($tagsInput)) {
                $tags->cod_agregar = $agregacionId;
                $tags->tag = $tagsInput;
                $tags->save();
            }

            // Redirigir al usuario a la estantería seleccionada
            $estanteria = Estanterias::findOne($agregacion->cod_estanterias);
            if ($estanteria !== null) {
                return $this->redirect(['site/estanteriaid', 'id' => $estanteria->id]);
            }
        }

        return $this->render('add-to-shelf', [
                    'agregacion' => $agregacion,
                    'estanterias' => $estanterias,
                    'tags' => $tags,
        ]);
    }

//FUNCIÓN QUE ACTUALIZA LAS PAGINAS LEÍDAS
    public function actionActualizarpaginasleidas($id) {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $libro = Libros::findOne($id);
        $estanteria = Estanterias::findOne(['nombre' => 'Leyendo']);

        $agregar = Agregacion::findOne(['cod_libro' => $id, 'cod_user' => Yii::$app->user->identity->id]);

        // Verificar el token CSRF
        Yii::$app->request->validateCsrfToken();

        if (Yii::$app->request->isPost) {
            $agregar->cod_estanterias = $estanteria->id;
            $agregar->paginasleidas = Yii::$app->request->post('paginasLeidas');

            if ($agregar->save(false)) {

                $porcentajeLeido = round($libro->porcentaje_leido($agregar->paginasleidas));

                return [
                    'success' => true,
                    'paginasleidas' => $agregar->paginasleidas,
                    'num_pag' => $libro->num_pag,
                    'porcentaje_leido' => $porcentajeLeido
                ];
            }
        }
    }

//FUNCION PARA MOVER A LA ESTANTERIA DE LEIDO
    public function actionActualizacion($id) {
        $libro = Libros::findOne($id);
        $estanteria = Estanterias::findOne(['nombre' => 'Leído']);
        $agregar = Agregacion::findOne(['cod_libro' => $id, 'cod_user' => Yii::$app->user->identity->id]);

        // Cambiar el código de estantería del libro a "Leído"
        $agregar->cod_estanterias = $estanteria->id;
        $agregar->paginasleidas = $libro->num_pag;

        // Actualizar la fecha de lectura
        $agregar->f_agregado = date("Y-m-d");

        $agregar->save(false);

        if ($estanteria !== null) {
            return $this->redirect(['site/estanteriaid', 'id' => $estanteria->id]);
        }
    }

}
