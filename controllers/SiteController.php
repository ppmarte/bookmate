<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\FormRegister;
use app\models\Users;
use app\models\User;
use app\models\Libros;
use app\models\Generos;
use app\models\Agregacion;
use app\models\Retos;
use app\models\Participaciones;
use app\models\Estanterias;
use app\models\Tags;
use app\models\Escritores;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\data\ActiveDataProvider;
use yii\web\JsonResponse;
use yii\web\JsExpression;
use yii\data\ArrayDataProvider;
use yii\db\Expression;
use yii\web\Session;
use app\models\FormRecoverPass;
use app\models\FormResetPass;

class SiteController extends Controller {

    public function actionEstanteria() {


        // Obtener las estanterías para mostrarlas en la vista
        $estanterias = Estanterias::find()->all();
        $this->view->title = 'Todos mis libros';
        $libros = Libros::find()
                ->joinWith([
                    'agregacions' => function ($query) {
                        $query->where(['agregacion.cod_user' => Yii::$app->user->identity->id]);
                    }
                ])
                ->all();

        //Mostrar el total de libros
        $cantidadLibro = Agregacion::find()
                ->select(['cod_estanterias', 'count(*) as total'])
                ->where(['cod_user' => Yii::$app->user->identity->id])
                ->groupBy(['cod_estanterias'])
                ->asArray()
                ->all();

        $cantidadLibros = [];
        foreach ($cantidadLibro as $item) {
            $cantidadLibros[$item['cod_estanterias']] = $item['total'];
        }

        $countLibros = Agregacion::find()
                ->select(['cod_user', 'count(*) as total'])
                ->where(['cod_user' => Yii::$app->user->identity->id])
                ->groupBy(['cod_user'])
                ->asArray()
                ->one();

        $dataProvider = new ArrayDataProvider([
            'allModels' => $libros,
        ]);

        //sección para los libros que estan en la estantería de leyenho
        $estanteriaLeyendoActualmente = Estanterias::findOne(['nombre' => 'Leyendo']);
        $estanteriaLeyendoActualmenteId = $estanteriaLeyendoActualmente->id;

        $librosLeyendoActualmente = Libros::find()
                ->joinWith([
                    'agregacions' => function ($query) use ($estanteriaLeyendoActualmenteId) {
                        $query->where([
                            'agregacion.cod_user' => Yii::$app->user->identity->id,
                            'agregacion.cod_estanterias' => $estanteriaLeyendoActualmenteId
                        ]);
                    }
                ])
                ->all();

        //Información sobre los retos.
        $model = Participaciones::find()->where(['cod_user' => Yii::$app->user->identity->id])->one();
        $librosusuario = Agregacion::find()->where(['cod_estanterias' => 2, 'cod_user' => Yii::$app->user->identity->id])
                        ->andWhere(['between', 'f_agregado', '2023-01-01', '2023-12-31'])->count();

        //Información sobre los tags.
        $tags = Tags::find()
                ->joinWith('codAgregar') // Unir con la relación "codAgregar" en el modelo Tags
                ->where([
                    'cod_user' => Yii::$app->user->identity->id,
                ])
                ->groupBy('tag')
                ->all();

        if ($librosusuario > 0) {
            $porcentaje = ($librosusuario / $model->objetivo) * 100;
        } else {
            $porcentaje = 0; // Para evitar división por cero si no hay participantes
        }
        // Renderizar la vista y pasar los datos a la vista
        return $this->render('estanteria', [
                    'dataProvider' => $dataProvider,
                    'estanterias' => $estanterias,
                    'cantidadLibros' => $cantidadLibros,
                    'countLibros' => $countLibros,
                    'librosLeyendoActualmente' => $librosLeyendoActualmente,
                    'model' => $model,
                    'librosusuario' => $librosusuario,
                    'porcentaje' => $porcentaje,
                    'tags' => $tags,
        ]);
    }

    public function actionEstanteriaid($id) {
        // Obtener las estanterías para mostrarlas en la vista
        $estanteria = Estanterias::find()->where(['id' => $id])->one();
        $estanterias = Estanterias::find()->all();

        if ($estanteria !== null) {
            $this->view->title = $estanteria->nombre;

            $libros = Libros::find()
                    ->joinWith([
                        'agregacions' => function ($query) use ($estanteria) {
                            $query->where([
                                'agregacion.cod_user' => Yii::$app->user->identity->id,
                                'agregacion.cod_estanterias' => $estanteria->id,
                            ]);
                        }
                    ])
                    ->all();

            //Cantidad de libros
            $countLibros = Agregacion::find()
                    ->select(['cod_user', 'count(*) as total'])
                    ->where(['cod_user' => Yii::$app->user->identity->id])
                    ->groupBy(['cod_user'])
                    ->asArray()
                    ->one();

            $cantidadLibro = Agregacion::find()
                    ->select(['cod_estanterias', 'count(*) as total'])
                    ->where(['cod_user' => Yii::$app->user->identity->id])
                    ->groupBy(['cod_estanterias'])
                    ->asArray()
                    ->all();

            $cantidadLibros = [];
            foreach ($cantidadLibro as $item) {
                $cantidadLibros[$item['cod_estanterias']] = $item['total'];
            }

            $dataProvider = new ArrayDataProvider([
                'allModels' => $libros,
            ]);

            //Libros leyendo actualmente
            $estanteriaLeyendoActualmente = Estanterias::findOne(['nombre' => 'Leyendo']);
            $estanteriaLeyendoActualmenteId = $estanteriaLeyendoActualmente->id;

            $librosLeyendoActualmente = Libros::find()
                    ->joinWith([
                        'agregacions' => function ($query) use ($estanteriaLeyendoActualmenteId) {
                            $query->where([
                                'agregacion.cod_user' => Yii::$app->user->identity->id,
                                'agregacion.cod_estanterias' => $estanteriaLeyendoActualmenteId,
                            ]);
                        }
                    ])
                    ->all();

            //Información sobre los retos
            $model = Participaciones::find()->where(['cod_user' => Yii::$app->user->identity->id])->one();
            $librosusuario = Agregacion::find()
                    ->where([
                        'cod_estanterias' => 2,
                        'cod_user' => Yii::$app->user->identity->id,
                    ])
                    ->andWhere(['between', 'f_agregado', '2023-01-01', '2023-12-31'])
                    ->count();

            if ($librosusuario > 0) {
                $porcentaje = ($librosusuario / $model->objetivo) * 100;
            } else {
                $porcentaje = 0;
            }

            //Información sobre los tags
            $tags = Tags::find()
                    ->joinWith('codAgregar')
                    ->where([
                        'cod_user' => Yii::$app->user->identity->id,
                    ])
                    ->groupBy('tag')
                    ->all();

            return $this->render('estanteria', [
                        'dataProvider' => $dataProvider,
                        'estanteria' => $estanteria,
                        'estanterias' => $estanterias,
                        'cantidadLibros' => $cantidadLibros,
                        'countLibros' => $countLibros,
                        'librosLeyendoActualmente' => $librosLeyendoActualmente,
                        'model' => $model,
                        'librosusuario' => $librosusuario,
                        'porcentaje' => $porcentaje,
                        'tags' => $tags,
            ]);
        }
    }

//Método para conseguir los libros filtranfo por los tags
    public function actionTag($tag) {


        $tag = Tags::find()->where(['tag' => $tag])->one();

        if ($tag !== null) {
            $this->view->title = 'Tus libros de ' . $tag->tag;

            $tags = Tags::find()
                    ->joinWith('codAgregar')
                    ->where([
                        'cod_user' => Yii::$app->user->identity->id,
                    ])
                    ->groupBy('tag')
                    ->all();
        }

        $libros = Libros::find()
                ->joinWith('agregacions.tags')
                ->where(['tags.tag' => $tag])
                ->all();

        $dataProvider = new ArrayDataProvider([
            'allModels' => $libros,
        ]);

        return $this->render('tag', [
                    'tags' => $tags,
                    'dataProvider' => $dataProvider
        ]);
    }

//Método para conseguir la información del reto de cada usuario
    public function actionTusretos() {
        $model = Participaciones::find()->where(['cod_user' => Yii::$app->user->identity->id])->one();

        $reto = Retos::find()->where(['nombre' => '2023'])->one();

        $dataProvider = new ActiveDataProvider([
            'query' => Agregacion::find()
                    ->where([
                        'cod_user' => Yii::$app->user->identity->id,
                        'cod_estanterias' => 2,
                    ])
                    ->andWhere(['between', 'f_agregado', '2023-01-01', '2023-12-31'])
        ]);

        $participantes = Participaciones::find()->where(['cod_reto' => 1])->count();
        $libros = Agregacion::find()->where(['cod_estanterias' => 2])->andWhere(['between', 'f_agregado', '2023-01-01', '2023-12-31'])->count();
        $librosusuario = Agregacion::find()->where(['cod_estanterias' => 2, 'cod_user' => Yii::$app->user->identity->id])
                        ->andWhere(['between', 'f_agregado', '2023-01-01', '2023-12-31'])->count();

        $paginas = Agregacion::find()
                ->joinWith('codLibro')
                ->where(['cod_estanterias' => 2])
                ->sum('libros.num_pag');

        if ($participantes > 0) {
            $mediaLibrosPorParticipante = $libros / $participantes;
        } else {
            $mediaLibrosPorParticipante = 0; // Para evitar división por cero si no hay participantes
        }

        return $this->render('tusretos', [
                    'reto' => $reto,
                    'model' => $model,
                    'dataProvider' => $dataProvider,
                    'participantes' => $participantes,
                    'libros' => $libros,
                    'mediaLibrosPorParticipante' => $mediaLibrosPorParticipante,
                    'librosusuario' => $librosusuario,
                    'paginas' => $paginas,
        ]);
    }

    //Método para mostrar los resultados de las busquedas y por generos
    public function actionBuscar() {

        $busqueda = Yii::$app->request->get('q');
        if (!$busqueda) {
            // Redirigir al usuario a la página de inicio o mostrar un mensaje de error
            return $this->goHome();
        }
        $registros = Libros::find()
                ->joinWith('codEscritors')
                ->where(['like', 'titulo', $busqueda])
                ->orWhere(['like', 'escritores.nombre', $busqueda])
                ->all();

        $generos = Generos::find()
                ->select('genero')
                ->distinct()
                ->all();

        return $this->render('resultado-busqueda', [
                    'registros' => $registros,
                    'busqueda' => $busqueda,
                    'generos' => $generos,
        ]);
    }

    // busqueda a tiempo real

    public function actionBusquedaajax($q) {
        $registros = Libros::find()
                ->joinWith('codEscritors')
                ->where(['like', 'titulo', $q])
                ->orWhere(['like', 'escritores.nombre', $q])
                ->all();

        return $this->asJson(['registros' => $registros]);
    }

//Método para mostrar los resultados de las busquedas por generos
    public function actionGenero($genero) {

        $this->view->title = 'Género: ' . $genero;

        $query = Libros::find()->joinWith('generos')->where(['generos.genero' => $genero]);
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $generos = Generos::find()
                ->select('genero')
                ->distinct()
                ->all();

        return $this->render('generos', [
                    'dataProvider' => $dataProvider,
                    'generos' => $generos,
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'user', 'admin'],
                'rules' => [
                    [
                        //El administrador tiene permisos sobre las siguientes acciones
                        'actions' => ['logout', 'admin'],
                        //Esta propiedad establece que tiene permisos
                        'allow' => true,
                        //Usuarios autenticados, el signo ? es para invitados
                        'roles' => ['@'],
                        //Este método nos permite crear un filtro sobre la identidad del usuario
                        //y así establecer si tiene permisos o no
                        'matchCallback' => function ($rule, $action) {
                            //Llamada al método que comprueba si es un administrador
                            return User::isUserAdmin(Yii::$app->user->identity->id);
                        },
                    ],
                    [
                        //Los usuarios simples tienen permisos sobre las siguientes acciones
                        'actions' => ['logout', 'user'],
                        //Esta propiedad establece que tiene permisos
                        'allow' => true,
                        //Usuarios autenticados, el signo ? es para invitados
                        'roles' => ['@'],
                        //Este método nos permite crear un filtro sobre la identidad del usuario
                        //y así establecer si tiene permisos o no
                        'matchCallback' => function ($rule, $action) {
                            //Llamada al método que comprueba si es un usuario simple
                            return User::isUserSimple(Yii::$app->user->identity->id);
                        },
                    ],
                ],
            ],
            //Controla el modo en que se accede a las acciones, en este ejemplo a la acción logout
            //sólo se puede acceder a través del método post
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions() {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex() {
        //Mostrar los 5 libros más nuevos
        $dataProvider = new ActiveDataProvider([
            'query' => Libros::find()
                    ->orderBy(['f_publi' => SORT_DESC])
                    ->limit(5),
            'pagination' => [
                'pageSize' => 5,
            ],
        ]);

        //Mostrar los 3 libros con mayor calificacion
        $mejoresLibros = new ActiveDataProvider([
            'query' => Libros::find()
                    ->joinWith('agregacions')
                    ->select(['libros.*', 'calificacion_media' => new Expression('AVG(agregacion.calificacion)')])
                    ->groupBy('libros.id')
                    ->orderBy([
                        'calificacion_media' => SORT_DESC,
                    ])
                    ->limit(3),
            'pagination' => [
                'pageSize' => 3,
            ],
        ]);

//Mostrar información de los retos
        $participantes = Participaciones::find()->where(['cod_reto' => 1])->count();
        $libros = Agregacion::find()->where(['cod_estanterias' => 2])->andWhere(['between', 'f_agregado', '2023-01-01', '2023-12-31'])->count();

        if ($participantes > 0) {
            $mediaLibrosPorParticipante = $libros / $participantes;
        } else {
            $mediaLibrosPorParticipante = 0; // Para evitar división por cero si no hay participantes
        }
        $participacion = null;
        if (!Yii::$app->user->isGuest && Yii::$app->user->identity !== null) {
            $participacion = Participaciones::find()->where(['cod_user' => Yii::$app->user->identity->id])->one();
        }


        $paginas = Agregacion::find()
                ->joinWith('codLibro')
                ->where(['cod_estanterias' => 2])
                ->sum('libros.num_pag');

        return $this->render('index', [
                    'dataProvider' => $dataProvider,
                    'mejoresLibros' => $mejoresLibros,
                    'participantes' => $participantes,
                    'libros' => $libros,
                    'mediaLibrosPorParticipante' => $mediaLibrosPorParticipante,
                    'paginas' => $paginas,
                    'participacion' => $participacion
        ]);
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin() {
        if (!\Yii::$app->user->isGuest) {

            if (User::isUserAdmin(Yii::$app->user->identity->id)) {
                return $this->redirect(["site/index"]);
            } else {
                return $this->redirect(["site/index"]);
            }
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {

            if (User::isUserAdmin(Yii::$app->user->identity->id)) {
                return $this->redirect(["site/index"]);
            } else {
                return $this->redirect(["site/index"]);
            }
        } else {
            return $this->render('login', [
                        'model' => $model,
            ]);
        }
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout() {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact() {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
                    'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout() {
        return $this->render('about');
    }

    public function actionPerfil() {

        //Pasar información sobre los usuarios
        $model = Users::find()->where(['id' => Yii::$app->user->identity->id])->one();
        $librosleidos = Agregacion::find()->where(['cod_user' => Yii::$app->user->identity->id, 'cod_estanterias' => 2])->count();

        $paginas = Agregacion::find()
                ->joinWith('codLibro')
                ->where(['cod_estanterias' => 2, 'cod_user' => Yii::$app->user->identity->id,])
                ->sum('libros.num_pag');

        if ($paginas == 0) {
            $paginas = 0;
        }

$cantidadLibros = [];

// Obtener la cantidad de libros por género y ordenarlos por la cantidad
$generos = Agregacion::find()
    ->joinWith('codLibro')
    ->joinWith('codLibro.generos')
    ->where(['cod_estanterias' => 2, 'cod_user' => Yii::$app->user->identity->id])
    ->select(['generos.genero', 'COUNT(*) AS total'])
    ->groupBy('generos.genero')
    ->orderBy(['total' => SORT_DESC])
    ->limit(3)
    ->asArray()
    ->all();

// Almacenar la cantidad de libros en el array $cantidadLibros
foreach ($generos as $item) {
    $cantidadLibros[$item['genero']] = $item['total'];
}

        $retos = Participaciones::find()
                ->where([ 'cod_user' => Yii::$app->user->identity->id])->count();


        return $this->render('perfil', [
                    'model' => $model,
                    'librosleidos' => $librosleidos,
                    'paginas' => $paginas,
                    'generos' => $generos,
            'cantidadLibros'=>$cantidadLibros,
            'retos'=>$retos,
        ]);
    }

    private function randKey($str = '', $long = 0) {
        $key = null;
        $str = str_split($str);
        $start = 0;
        $limit = count($str) - 1;
        for ($x = 0; $x < $long; $x++) {
            $key .= $str[rand($start, $limit)];
        }
        return $key;
    }

    public function actionConfirm() {
        $table = new Users;
        if (Yii::$app->request->get()) {

            //Obtenemos el valor de los parámetros get
            $id = Html::encode($_GET["id"]);
            $authKey = $_GET["authKey"];

            if ((int) $id) {
                //Realizamos la consulta para obtener el registro
                $model = $table
                        ->find()
                        ->where("id=:id", [":id" => $id])
                        ->andWhere("authKey=:authKey", [":authKey" => $authKey]);

                //Si el registro existe
                if ($model->count() == 1) {
                    $activar = Users::findOne($id);
                    $activar->activate = 1;
                    if ($activar->update()) {
                        echo "Enhorabuena registro llevado a cabo correctamente, redireccionando ...";
                        echo "<meta http-equiv='refresh' content='8; " . Url::toRoute("site/login") . "'>";
                    } else {
                        echo "Ha ocurrido un error al realizar el registro, redireccionando ...";
                        echo "<meta http-equiv='refresh' content='8; " . Url::toRoute("site/login") . "'>";
                    }
                } else { //Si no existe redireccionamos a login
                    return $this->redirect(["site/login"]);
                }
            } else { //Si id no es un número entero redireccionamos a login
                return $this->redirect(["site/login"]);
            }
        }
    }

    public function actionRegister() {
        $ultimaId = Users::find()->max('id');
        $id = $ultimaId + 1;

        //Creamos la instancia con el model de validación
        $model = new FormRegister;

        //Mostrará un mensaje en la vista cuando el usuario se haya registrado
        $msg = null;

        //Validación mediante ajax
        if ($model->load(Yii::$app->request->post()) && Yii::$app->request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }

        //Validación cuando el formulario es enviado vía post
        //Esto sucede cuando la validación ajax se ha llevado a cabo correctamente
        //También previene por si el usuario tiene desactivado javascript y la
        //validación mediante ajax no puede ser llevada a cabo
        if ($model->load(Yii::$app->request->post())) {
            if ($model->validate()) {
                $table = new Users;
                $table->username = $model->username;
                $table->email = $model->email;
                $table->f_registro = date("Y-m-d");
                $table->biografia = $model->biografia;
                //Encriptamos el password
                $table->password = crypt($model->password, Yii::$app->params["salt"]);
                //Creamos una cookie para autenticar al usuario cuando decida recordar la sesión, esta misma
                //clave será utilizada para activar el usuario
                $table->authKey = $this->randKey("abcdef0123456789", 20);
                //Creamos un token de acceso único para el usuario
                $table->accessToken = $this->randKey("abcdef0123456789", 20);

                $avatar = \yii\web\UploadedFile::getInstance($model, 'file');
                if (!is_null($avatar)) {
                    $path = 'img/user/' . $id . '.' . $avatar->extension;
                    $fullPath = Yii::getAlias('@webroot') . '/' . $path;

                    // Guardar la imagen en la carpeta
                    $avatar->saveAs($fullPath);

                    // Actualizar el atributo de imagen del modelo
                    $table->file = $path;
                    //Si el registro es guardado correctamente
                }
                if ($table->insert()) {
                    //Nueva consulta para obtener el id del usuario
                    //Para confirmar al usuario se requiere su id y su authKey
                    $user = $table->find()->where(["email" => $model->email])->one();
                    $id = urlencode($user->id);
                    $authKey = urlencode($user->authKey);

                    //Obtener la URL base del proyecto
                    $baseUrl = Url::home(true);

                    //Construir la URL de confirmación
                    $confirmUrl = "{$baseUrl}site/confirm?id={$id}&authKey={$authKey}";

                    $subject = "Confirmar registro";
                    $body = "<h1>Termina tu registro en BookMate, haz click en siguiente enlace</h1>";
                    $body .= "<a href='{$confirmUrl}'>Confirmar</a>";

                    //Enviamos el correo
                    Yii::$app->mailer->compose()
                            ->setTo($user->email)
                            ->setFrom([Yii::$app->params["adminEmail"] => Yii::$app->params["title"]])
                            ->setSubject($subject)
                            ->setHtmlBody($body)
                            ->send();

                    $model->username = null;
                    $model->email = null;
                    $model->password = null;
                    $model->password_repeat = null;
                    $model->biografia = null;

                    $msg = "Enhorabuena, ahora sólo falta que confirmes tu registro en tu cuenta de correo";
                } else {
                    $msg = "Ha ocurrido un error al llevar a cabo tu registro";
                }
            } else {
                $model->getErrors();
            }
        }
        return $this->render("register", ["model" => $model, "msg" => $msg]);
    }

//Método para mostrar los escritores
    public function actionEscritores() {
        $dataProvider = new ActiveDataProvider([
            'query' => Escritores::find(),
            'pagination' => [
                'pageSize' => 12
            ],
            'sort' => [
                'defaultOrder' => [
                    'nombre' => SORT_ASC,
                ]
            ],
        ]);

        return $this->render('escritores', [
                    'dataProvider' => $dataProvider,
        ]);
    }

//Método para mostrar los libros
    public function actionLibros() {
        $dataProvider = new ActiveDataProvider([
            'query' => Libros::find(),
            'pagination' => [
                'pageSize' => 15
            ],
            'sort' => [
                'defaultOrder' => [
                    'titulo' => SORT_ASC,
                ]
            ],
        ]);

        return $this->render('libros', [
                    'dataProvider' => $dataProvider,
        ]);
    }

//Método para recuperar la contraseña
    public function actionRecoverpass() {
        //Instancia para validar el formulario
        $model = new FormRecoverPass;

        //Mensaje que será mostrado al usuario en la vista
        $msg = null;

        if ($model->load(Yii::$app->request->post())) {
            if ($model->validate()) {
                //Buscar al usuario a través del email
                $table = Users::find()->where("email=:email", [":email" => $model->email]);

                //Si el usuario existe
                if ($table->count() == 1) {
                    //Crear variables de sesión para limitar el tiempo de restablecido del password
                    //hasta que el navegador se cierre
                    $session = new Session;
                    $session->open();

                    //Esta clave aleatoria se cargará en un campo oculto del formulario de reseteado
                    $session["recover"] = $this->randKey("abcdef0123456789", 20);
                    $recover = $session["recover"];

                    //También almacenaremos el id del usuario en una variable de sesión
                    //El id del usuario es requerido para generar la consulta a la tabla users y 
                    //restablecer el password del usuario
                    $table = Users::find()->where("email=:email", [":email" => $model->email])->one();
                    $session["id_recover"] = $table->id;

                    //Esta variable contiene un número hexadecimal que será enviado en el correo al usuario 
                    //para que lo introduzca en un campo del formulario de reseteado
                    //Es guardada en el registro correspondiente de la tabla users
                    $verification_code = $this->randKey("abcdef0123456789", 8);
                    //Columna verification_code
                    $table->verification_code = $verification_code;
                    //Guardamos los cambios en la tabla users
                    $table->save();

                    //Obtener la URL base del proyecto
                    $baseUrl = Url::home(true);

                    //Construir la URL de confirmación
                    $confirmUrl = "{$baseUrl}site/resetpass";
                    //Creamos el mensaje que será enviado a la cuenta de correo del usuario
                    $subject = "Recuperar password";
                    $body = "<p>Copie el siguiente código de verificación para restablecer su password ... ";
                    $body .= "<strong>" . $verification_code . "</strong></p>";
                    $body .= "<p><a href='{$confirmUrl}'>Recuperar password</a></p>";

                    //Enviamos el correo
                    Yii::$app->mailer->compose()
                            ->setTo($model->email)
                            ->setFrom([Yii::$app->params["adminEmail"] => Yii::$app->params["title"]])
                            ->setSubject($subject)
                            ->setHtmlBody($body)
                            ->send();

                    //Vaciar el campo del formulario
                    $model->email = null;

                    //Mostrar el mensaje al usuario
                    $msg = "Le hemos enviado un mensaje a su cuenta de correo para que pueda resetear su password";
                } else { //El usuario no existe
                    $msg = "Ha ocurrido un error";
                }
            } else {
                $model->getErrors();
            }
        }
        return $this->render("recoverpass", ["model" => $model, "msg" => $msg]);
    }

    //Método para cambiar la contraseña
    public function actionResetpass() {
        //Instancia para validar el formulario
        $model = new FormResetPass;

        //Mensaje que será mostrado al usuario
        $msg = null;

        //Abrimos la sesión
        $session = new Session;
        $session->open();

        //Si no existen las variables de sesión requeridas lo expulsamos a la página de inicio
        if (empty($session["recover"]) || empty($session["id_recover"])) {
            return $this->redirect(["site/index"]);
        } else {

            $recover = $session["recover"];
            //El valor de esta variable de sesión la cargamos en el campo recover del formulario
            $model->recover = $recover;

            //Esta variable contiene el id del usuario que solicitó restablecer el password
            //La utilizaremos para realizar la consulta a la tabla users
            $id_recover = $session["id_recover"];
        }

        //Si el formulario es enviado para resetear el password
        if ($model->load(Yii::$app->request->post())) {
            if ($model->validate()) {
                //Si el valor de la variable de sesión recover es correcta
                if ($recover == $model->recover) {
                    //Preparamos la consulta para resetear el password, requerimos el email, el id 
                    //del usuario que fue guardado en una variable de session y el código de verificación
                    //que fue enviado en el correo al usuario y que fue guardado en el registro
                    $table = Users::findOne(["email" => $model->email, "id" => $id_recover, "verification_code" => $model->verification_code]);

                    //Encriptar el password
                    $table->password = crypt($model->password, Yii::$app->params["salt"]);

                    //Si la actualización se lleva a cabo correctamente
                    if ($table->save()) {

                        //Destruir las variables de sesión
                        $session->destroy();

                        //Vaciar los campos del formulario
                        $model->email = null;
                        $model->password = null;
                        $model->password_repeat = null;
                        $model->recover = null;
                        $model->verification_code = null;

                        $msg = "Enhorabuena, password reseteado correctamente, redireccionando a la página de login ...";
                        $msg .= "<meta http-equiv='refresh' content='5; " . Url::toRoute("site/login") . "'>";
                    } else {
                        $msg = "Ha ocurrido un error";
                    }
                } else {
                    $model->getErrors();
                }
            }
        }

        return $this->render("resetpass", ["model" => $model, "msg" => $msg]);
    }

    public function actionPolitica() {
        return $this->render('politica');
    }

    public function actionTerminos() {
        return $this->render('terminos');
    }

}
