<?php

namespace app\controllers;

use app\models\Users;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use Yii;

/**
 * UsersController implements the CRUD actions for Users model.
 */
class UsersController extends Controller {

    /**
     * @inheritDoc
     */
    public function behaviors() {
        return array_merge(
                parent::behaviors(),
                [
                    'verbs' => [
                        'class' => VerbFilter::className(),
                        'actions' => [
                            'delete' => ['POST'],
                        ],
                    ],
                ]
        );
    }

    /**
     * Lists all Users models.
     *
     * @return string
     */
    public function actionIndex() {
        $dataProvider = new ActiveDataProvider([
            'query' => Users::find(),
                /*
                  'pagination' => [
                  'pageSize' => 50
                  ],
                  'sort' => [
                  'defaultOrder' => [
                  'id' => SORT_DESC,
                  ]
                  ],
                 */
        ]);

        return $this->render('index', [
                    'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Users model.
     * @param int $id ID
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id) {
        return $this->render('view', [
                    'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Users model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate() {
           $model = new Users();
        $avatar = \yii\web\UploadedFile::getInstance($model, 'file');

        if ($model->load($this->request->post()) && $model->save()) {
            if (!is_null($avatar)) {
                $path = 'img/user/' . $model->id . '.' . $avatar->extension;
                $fullPath = Yii::getAlias('@web') . '/' . $path;

                // Guardar la imagen en la carpeta
                $avatar->saveAs($fullPath);

                // Actualizar el atributo de imagen del modelo
                $model->file = $path;
            }
            $model->password = crypt($model->password, Yii::$app->params["salt"]);
            $model->save(false); // Guardar sin validar las reglas del modelo
            // Redireccionar a la página correspondiente o mostrar un mensaje de éxito
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
                    'model' => $model,
        ]);

    }

    /**
     * Updates an existing Users model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $id ID
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id) {
        $model = $this->findModel($id);
        $avatar = \yii\web\UploadedFile::getInstance($model, 'file');

        if ($model->load($this->request->post()) && $model->save()) {
            if (!is_null($avatar)) {
                $path = 'img/user/' . $model->id . '.' . $avatar->extension;
                $fullPath = Yii::getAlias('@webroot') . '/' . $path;

                // Guardar la imagen en la carpeta
                $avatar->saveAs($fullPath);

                // Actualizar el atributo de imagen del modelo
                $model->file = $path;
            }
            $model->password = crypt($model->password, Yii::$app->params["salt"]);
            $model->save(false); // Guardar sin validar las reglas del modelo
            // Redireccionar a la página correspondiente o mostrar un mensaje de éxito
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
                    'model' => $model,
        ]);
    }

//Método para que un usuario modifique su perfil
    public function actionPerfil($id) {
        $model = $this->findModel($id);
        $avatar = \yii\web\UploadedFile::getInstance($model, 'file');

        if ($model->load($this->request->post()) && $model->save()) {
            if (!is_null($avatar)) {
                $path = 'img/user/' . $model->id . '.' . $avatar->extension;
                $fullPath = Yii::getAlias('@webroot') . '/' . $path;

                // Guardar la imagen en la carpeta
                $avatar->saveAs($fullPath);

                // Actualizar el atributo de imagen del modelo
                $model->file = $model->id . '.' . $avatar->extension;
            }
            
            $model->password = crypt($model->password, Yii::$app->params["salt"]);


            $model->save(false); // Guardar sin validar las reglas del modelo
            // Redireccionar a la página correspondiente o mostrar un mensaje de éxito
            return $this->redirect(['site/perfil', 'id' => $model->id]);
        }

        return $this->render('perfil', [
                    'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Users model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $id ID
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id) {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Users model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $id ID
     * @return Users the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = Users::findOne(['id' => $id])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    //metodo para que un usuario pueda desactivar su cuenta
    public function actionInactivar($id) {
        $model = $this->findModel($id);

        $model->activate = 0;
        $model->save();
        return $this->redirect(['site/index']);
    }

}


