<?php

namespace app\controllers;

use app\models\Escritores;
use app\models\Generos;
use app\models\Escriben;
use app\models\Estanterias;
use app\models\Agregacion;
use app\models\Libros;
use app\models\Leen;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\web\Response;
use yii;
use yii\helpers\Url;

/**
 * LibrosController implements the CRUD actions for Libros model.
 */
class LibrosController extends Controller {

    /**
     * @inheritDoc
     */
    public function behaviors() {
        return array_merge(
                parent::behaviors(),
                [
                    'verbs' => [
                        'class' => VerbFilter::className(),
                        'actions' => [
                            'delete' => ['POST'],
                        ],
                    ],
                ]
        );
    }

    /**
     * Lists all Libros models.
     *
     * @return string
     */
    public function actionIndex() {
        $dataProvider = new ActiveDataProvider([
            'query' => Libros::find(),
                /*
                  'pagination' => [
                  'pageSize' => 50
                  ],
                  'sort' => [
                  'defaultOrder' => [
                  'id' => SORT_DESC,
                  ]
                  ],
                 */
        ]);

        return $this->render('index', [
                    'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Libros model.
     * @param int $id ID
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id) {
        return $this->render('view', [
                    'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Libros model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate() {
        $model = new Libros();
        $generos = Generos::find()->orderBy('genero')->all();

        $generosSeleccionados = [];
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            // Obtener el ID recién creado del libro
            $id = $model->getPrimaryKey();

            // Crear una nueva instancia de la clase "Escribir"
            $escribir = new Escriben();
            $escribir->cod_libro = $id;
            $escribir->cod_escritor = Yii::$app->request->post('Libros')['cod_escritor'];
            $escribir->save();

            // Obtener los géneros seleccionados

            $generos = Yii::$app->request->post('Libros')['generos'];
            foreach ($generos as $genero) {
                $libroGenero = new Generos();
                $libroGenero->cod_libro = $id;
                $libroGenero->genero = $genero;
                $libroGenero->save();
            }
            return $this->redirect(['view', 'id' => $model->id]);
        }

        // Consultar los géneros y pasarlos a la vista
        $generosArray = ArrayHelper::map($generos, 'genero', 'genero');

        return $this->render('create', [
                    'model' => $model,
                    'generos' => $generosArray,
                    'generosSeleccionados' => $generosSeleccionados,
        ]);
    }

    /**
     * Updates an existing Libros model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $id ID
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id) {
        $model = $this->findModel($id);

        // Obtener el autor actual del libro
        $autores = $model->codEscritors;
        $autorActual = null;
        if (!empty($autores)) {
            // Obtener el primer autor (pueden haber múltiples autores)
            $autorActual = $autores[0]->id;
        }

        // Obtener los géneros disponibles
        $generos = Generos::find()->distinct()->orderBy('genero')->all();

        // Obtener los géneros seleccionados
        $generosSeleccionados = isset($model->generos) ? ArrayHelper::getColumn($model->generos, 'genero') : [];
        $generosActuales = ArrayHelper::getColumn($model->generos, 'genero');

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            // Actualizar la clave foránea en la tabla "escribir"
            $escribir = Escriben::findOne(['cod_libro' => $id]);

            if (!$escribir) {
                // Si no existe una entrada en la tabla "escribir", crear una nueva instancia
                $escribir = new Escriben();
                $escribir->cod_libro = $id;
            }

            // Asignar el código del autor proporcionado en el formulario de actualización
            $codEscritor = Yii::$app->request->post('Libros')['cod_escritor'];

            // Si el campo oculto "$autorActual" está presente, usar su valor en lugar del valor del campo "cod_escritor"
            $autorActual = Yii::$app->request->post('autorActual');
            if ($codEscritor != $autorActual) {
                $autorActual = $codEscritor;
            }
            if (isset($autorActual)) {
                $codEscritor = $autorActual;
            }


            // Asignar el código del autor al modelo "$model"
            $model->cod_escritor = $codEscritor;
            // Guardar los cambios en el modelo
            $model->save();
            // Actualizar la entrada en la tabla "escribir"
            $escribir->cod_escritor = $codEscritor;
            $escribir->save();

            // Guardar los nuevos géneros
            $generos = Yii::$app->request->post('Libros')['generos'];

            foreach ($generos as $genero) {
                $librosGeneros = new Generos();
                $librosGeneros->cod_libro = $id;
                $librosGeneros->genero = $genero;
                $librosGeneros->save();
            }

            return $this->redirect(['view', 'id' => $model->id]);
        }

        // Obtener los géneros y pasarlos a la vista
        $generosArray = ArrayHelper::map($generos, 'genero', 'genero');

// Recorrer los géneros y agregar la propiedad "checked" si ya están seleccionados
        foreach ($generosArray as $key => $genero) {
            if (in_array($genero, $generosActuales)) {
                $generosArray[$key] = ['label' => $genero, 'checked' => true];
            } else {
                $generosArray[$key] = ['label' => $genero, 'checked' => false];
            }
        }

        return $this->render('update', [
                    'model' => $model,
                    'generos' => $generosArray,
                    'generosSeleccionados' => $generosSeleccionados,
                    'autorActual' => $autorActual, // Pasar el autor actual a la vista
                    'generosActuales' => $generosActuales, // Pasar los géneros actuales a la vista
        ]);
    }

    /**
     * Deletes an existing Libros model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $id ID
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id) {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Libros model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $id ID
     * @return Libros the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = Libros::findOne(['id' => $id])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

//Metodo para actualizar la calificación que le da un usuario al libro desde su vista.
    public function actionActualizarcalificacion($id) {
        $agregacion = Agregacion::findOne(['cod_libro' => $id, 'cod_user' => Yii::$app->user->identity->id]);

        if (!$agregacion) {
            $agregacion = new Agregacion();
            $agregacion->cod_libro = $id;
            $agregacion->cod_user = Yii::$app->user->identity->id;
            $agregacion->cod_estanterias = 3;
            $agregacion->f_agregado = date("Y-m-d");
        }

        $calificacion = Yii::$app->request->post('calificacion');
        $agregacion->calificacion = $calificacion;

        if ($agregacion->save()) {
    //Una vez actualizada la calificación, nos redirige de nuevo a la página, recargándola y mostrando la calificación añadida
            return $this->redirect(['view', 'id' => $id]);
        } else {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['success' => false, 'errors' => $agregacion->getErrors()];
        }
    }

}
