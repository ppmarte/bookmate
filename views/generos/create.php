<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Generos $model */

$this->title = 'Nuevo Género';
$this->params['breadcrumbs'][] = ['label' => 'Géneros', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="generos-create administradores">

    <h2><?= Html::encode($this->title) ?></h2>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
