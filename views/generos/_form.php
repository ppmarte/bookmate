<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var app\models\Generos $model */
/** @var yii\widgets\ActiveForm $form */
?>

<div class="generos-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'cod_libro')->textInput() ?>

    <?= $form->field($model, 'genero')->textInput(['maxlength' => true, 'placeholder' => 'Escribe el género del libro']) ?>

    <div class="form-group">
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
