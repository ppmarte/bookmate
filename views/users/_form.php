<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;
use kartik\file\FileInput;
use kartik\editors\Summernote;
/** @var yii\web\View $this */
/** @var app\models\Users $model */
/** @var yii\widgets\ActiveForm $form */
?>

<div class="users-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'username')->textInput(['maxlength' => true, 'placeholder' => 'Introduce tu nombre de usuario']) ?>

<div class="form-group">
 <?= $form->field($model, "email")->textInput(['placeholder' => 'Introduce tu correo electrónico']) ?>   
</div>

<div class="form-group">
<?= $form->field($model, "password")->passwordInput(['placeholder' => 'Introduce tu contraseña']) ?>
  
</div>

<div class="form-group">
 <?= $form->field($model, "password_repeat")->passwordInput(['placeholder' => 'Repite tu contraseña'])  ?>   
</div>

    <?=
    $form->field($model, 'f_registro', [
        'labelOptions' => ['label' => 'Fecha de registro']
    ])->widget(DatePicker::classname(), [
        'name' => 'fecha de registro',
        'convertFormat' => true,
        'disabled' => true,
        'options' => [
            'value' =>$model->f_registro,
        ]
    ])
    ?>

    <div class="form-group">
        <?=
        $form->field($model, 'file', [
   'labelOptions' => ['label' => 'Avatar']])->widget(FileInput::classname(), [
            'name' => 'attachment_50',
            'pluginOptions' => [
                'accept' => 'file/*',
                'showPreview' => false,
                'showCaption' => true,
                'showRemove' => true,
            ]
        ])
        ?>

    </div>

<div class="form-group">
     <?= $form->field($model, 'biografia')->widget(Summernote::class, [
 'useKrajeePresets' => true,
        'pluginOptions'=>[
    'height' => 200,
    'dialogsFade' => true,
    'toolbar' => [
        ['style1', ['style']],
        ['style2', ['bold', 'italic', 'underline', 'strikethrough', 'superscript', 'subscript']],
        ['font', ['fontname', 'fontsize', 'color', 'clear']],
        ['para', ['ul', 'ol', 'paragraph', 'height']],
        ['insert', ['link', 'picture', 'video', 'table', 'hr']],
    ],
    'fontSizes' => ['8', '9', '10', '11', '12', '13', '14', '16', '18', '20', '24', '36', '48'],
    'codemirror' => [
      //  'theme' => Codemirror::DEFAULT_THEME,
        'lineNumbers' => true,
        'styleActiveLine' => true,
        'matchBrackets' => true,
        'smartIndent' => true,
        'enablePrettyFormat'=>false,
        'autoFormatCode'=>false,
        
    ],
             'placeholder' => 'Ingrese su biografía aquí',
]

]);?>

</div>

    <div class="form-group">
<?= Html::submitButton('Guardar', ['class' => 'btn btn-success']) ?>
    </div>

<?php ActiveForm::end(); ?>

</div>
