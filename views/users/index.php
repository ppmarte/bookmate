<?php

use app\models\Users;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/** @var yii\web\View $this */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = 'Usuarios';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="users-index admintabla">

    <h2><?= Html::encode($this->title) ?></h12>



    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [

            'username',
            'email:email',
            [
            'attribute' => 'biografia',
            'format' => 'raw',
            ],
            //'password',
           // 'authKey',
            //'accessToken',
            //'activate',
            //'f_registro',
            //'biografia',
            [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, Users $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'id' => $model->id]);
                 }
            ],
        ],
    ]); ?>


</div>
