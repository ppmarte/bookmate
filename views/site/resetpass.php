<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
?>
  <div class="site-login superior">
    <div class="formularioDiferente">
<h4><?= $msg ?></h4>
<h2>Cambiar contraseña</h2>
<?php $form = ActiveForm::begin([
    'method' => 'post',
    'enableClientValidation' => true,
]);
?>

<div class="form-group">
 <?= $form->field($model, 'email')->label('Email')->textInput(['email','placeholder' => 'Introduce tu email']) ?>  
</div>
 
<div class="form-group">
 <?= $form->field($model, 'password')->label('Contraseña')->passwordInput(['placeholder' => 'Introduce tu contraseña']) ?>  
</div>
 
<div class="form-group">
 <?= $form->field($model, 'password_repeat')->label('Repite la Contraseña')->passwordInput(['placeholder' => 'Repite tu contraseña']) ?>  
</div>

<div class="form-group">
 <?= $form->field($model, 'verification_code')->label('Código de verificación')->textInput(['placeholder' => 'Introduce el código de verificación'])  ?>  
</div>

<div class="form-group">
 <?= $form->field($model, 'recover')->input('hidden')->label(false) ?>  
</div>
 
<?= Html::submitButton('Cambiar contraseña', ['class' => 'btn btn-primary']) ?>
 
<?php $form->end() ?>
</div></div>