<?php

use app\models\Escritores;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\widgets\ListView;

/** @var yii\web\View $this */
/** @var yii\data\ActiveDataProvider $dataProvider */
$this->title = 'Nuestros escritores';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="escritores-index superior">

    <h2><?= Html::encode($this->title) ?></h2>

    <?=
    ListView::widget([
        'dataProvider' => $dataProvider,
        'itemView' => '_autor',
        'layout' => "\n{items}\n\n{pager}",
    ]);
    ?>

</div>
