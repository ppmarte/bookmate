<?php
use yii\helpers\Html;
use  yii\widgets\ListView;
use  yii\widgets\LinkPager;
use kartik\widgets\StarRating;
/** @var yii\web\View $this */

$this->title = 'Todos nuestros libros';
?>

<div class="site-libros superior">
    
     <h2><?= Html::encode($this->title) ?></h2>
     
     
      <?=   ListView::widget([
            'dataProvider' => $dataProvider,
          
             'itemView' => '_libro',
              'layout'=>"\n{items}\n\n{pager}",

          ]);
  ?>
</div>