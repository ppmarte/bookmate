<?php

/** @var yii\web\View $this */
/** @var yii\bootstrap4\ActiveForm $form */
/** @var app\models\LoginForm $model */

use yii\bootstrap4\ActiveForm;
use yii\bootstrap4\Html;

$this->title = 'Conectarse';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-login superior">
    <div class="formularioDiferente">
        <h2><?= Html::encode($this->title) ?></h2>

    <p>Rellena los campos para conectarte:</p>

    <?php $form = ActiveForm::begin([
        'id' => 'login-form',
        'layout' => 'horizontal',
        'fieldConfig' => [
            'template' => "{label}\n{input}\n{error}",
            'labelOptions' => ['class' => 'col-lg-3 col-form-label mr-lg-3'],
            'inputOptions' => ['class' => 'col-lg-7 form-control'],
            'errorOptions' => ['class' => 'col-lg-9 invalid-feedback'],
        ],
    ]); ?>

        <?= $form->field($model, 'username')->label('Usuario')->textInput(['autofocus' => true,'placeholder' => 'Introduce tu nombre de usuario']) ?>

        <?= $form->field($model, 'password')->label('Contraseña')->passwordInput(['placeholder' => 'Introduce tu contraseña']) ?>

            
        <?= $form->field($model, 'rememberMe')->checkbox([
            'template' => "<div class=\"offset-lg-1 col-lg-5 custom-control custom-checkbox\">{input} {label}</div>\n<div class=\"col-lg-8\">{error}</div>",
            
        ]) ?>

        <div class="form-group">
            <div class="offset-lg-4 col-lg-12">
                <?= Html::submitButton('Conectarse', ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>  
            </div>
    
          
        </div>
       <div class="form-group">
           <p> <?= Html::a('¿Has olvidado la Contraseña?',['recoverpass' ]) ?> </p>
           <p>¿Aún no tienes cuenta? <?= Html::a('Crea tu cuenta',['register' ]) ?> </p>
            </div>
    

    <?php ActiveForm::end(); ?>

    </div>
</div>
