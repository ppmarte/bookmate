<?php

/** @var yii\web\View $this */
/** @var yii\bootstrap4\ActiveForm $form */
/** @var app\models\ContactForm $model */

use yii\bootstrap4\ActiveForm;
use yii\bootstrap4\Html;
use yii\captcha\Captcha;

$this->title = 'Contáctanos';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-contact superior">
    <div class="formularioDiferente"><h2><?= Html::encode($this->title) ?></h2>

    <?php if (Yii::$app->session->hasFlash('contactFormSubmitted')): ?>

        <div class="alert alert-success">
           Gracias por ponerte en contacto con nosotros. Te responderemos con la mayor brevedad posible.
        </div>

       

    <?php else: ?>

        <p>
           Si tienes alguna duda o sugerencia, por favor, ponte en contacto con nosotros. Gracias. 
           
        </p>

        <div class="row">
            <div class="col-lg-11">

                <?php $form = ActiveForm::begin(['id' => 'contact-form']); ?>

                    <?= $form->field($model, 'name', [
   'labelOptions' => ['label' => 'Nombre']])->textInput(['autofocus' => true]) ?>

                    <?= $form->field($model, 'email') ?>

                    <?= $form->field($model, 'subject', [
   'labelOptions' => ['label' => 'Asunto']]) ?>

                    <?= $form->field($model, 'body', [
   'labelOptions' => ['label' => 'Su mensaje']])->textarea(['rows' => 6]) ?>

                    <?= $form->field($model, 'verifyCode')->widget(Captcha::className(), [
                        'template' => '<div class="row"><div class="col-lg-5">{image}</div><div class="col-lg-6">{input}</div></div>',
                    ]) ?>

                    <div class="form-group">
                        <?= Html::submitButton('Enviar', ['class' => 'btn btn-primary', 'name' => 'contact-button']) ?>
                    </div>

                <?php ActiveForm::end(); ?>

            </div>
        </div>

    <?php endif; ?>
        </div>
</div>
