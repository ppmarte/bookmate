
<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
?>


<div class="col-sm-12">
    <div class="card minimo">

        <?= Html::a(Html::img('@web/img/' . $model->cod_libro . '.jpg', ['alt' => 'Portada', 'class' => 'book cover']), ['libros/view', 'id' => $model->cod_libro]) ?>

    </div>
</div>