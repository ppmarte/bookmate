<?php

use yii\helpers\Html;
use yii\widgets\ListView;
use yii\widgets\LinkPager;
use kartik\widgets\StarRating;

/** @var yii\web\View $this */
$this->title = 'BookMate';
?>
<!-- Header -->
<div id="portada">
    <h1>
        <?= Html::encode($this->title) ?>
    </h1>
</div>
<!-- Fin header -->
<div class="site-index superior"> 
    <!-- Inicio mejores libros -->
    <div class="bg">
        <h2>Los mejor valorados</h2>  

        <div class="hero-section">

            <div class="card-grid">
                <?php foreach ($mejoresLibros->getModels() as $index => $libro): ?>
                    <a class="card2" href="<?= Yii::$app->urlManager->createUrl(['libros/view', 'id' => $libro->id]) ?>">
                        <div class="card_background"
                             style="background: url('<?= Yii::getAlias('@web') ?>/img/<?= $libro->id ?>.jpg'); background-size: cover;    background-position:center;">
                        </div>

                        <div class="card_content">

                            <h3 class="card_heading">
                                <?=
                                StarRating::widget([
                                    'name' => 'rating_33',
                                    'value' => $libro->calificacionMedia,
                                    'pluginOptions' => [
                                        'size' => 'sm',
                                        'readonly' => true,
                                        'showClear' => false,
                                        'showCaption' => false,
                                        'language' => 'es',
                                    ],
                                ]);
                                ?>
                            </h3>
                        </div>
                    </a>
                <?php endforeach; ?>
            </div>
        </div>

    </div>
    <!-- Fin Mejores libros -->
    <div class="separador"></div>


    <!-- Inicio Info reto -->
    <div class="reto-actual">
        <?= Html::img('@web/img/icon/icon2.png', ['alt' => 'IconoReto', 'class' => 'bookImg']) ?>
        <div class="year">
            Reto de Lectura 2023
        
        <span>¿Quieres marcarte un objetivo de lectura este año?</span>
            <?php
            if (!Yii::$app->user->isGuest) {
                if (!empty($participacion) && isset($participacion->objetivo)) {
                    // $participacion es un objeto y tiene la propiedad 'objetivo'
                    echo Html::a('Actualizar tu objetivo', ['participaciones/reto'], ['class' => 'btn btn-success']);
                } else {
                    // $participacion no es un objeto o no tiene la propiedad 'objetivo'
                    echo Html::a('Participar', ['participaciones/reto'], ['class' => 'btn btn-success']);
                }
            }
            ?>



        </div>
        <!-- Información sobre los retos -->
        <div class="challengeStats">

            <div class="challengeStatItem">Participantes</div>
            <div class="statNumber">
                <?= $participantes ?>
            </div>
            <div class="challengeStatItem">Libros leídos</div>
            <div class="statNumber">
                <?= $libros ?>
            </div>
            <div class="challengeStatItem">Media de Libros</div>
            <div class="statNumber">
                <?= $mediaLibrosPorParticipante ?>
            </div>
            <div class="challengeStatItem">Páginas leídas</div>
            <div class="statNumber">
                <?= $paginas ?>
            </div>
        </div>
    </div>
    <!-- Fin retp -->
    <!-- Inicio novedades -->
    <div class="bg">
        <h2>Nuestras novedades</h2>

        <?=
        ListView::widget([
            'dataProvider' => $dataProvider,
            'itemView' => '_libro',
            'layout' => "\n{items}",
        ]);
        ?>
        <?php
        if (!Yii::$app->user->isGuest) {
            echo Html::a('Ver más', ['libros'], ['class' => 'btn btn-success centrado']);
        }
        ?>

    </div>
    <!-- Fin novedades -->
</div>