<?php
use yii\helpers\Html;
use  yii\widgets\ListView;
use  yii\widgets\LinkPager;
/** @var yii\web\View $this */


$this->params['breadcrumbs'][] = $this->title;
?>
<div class="generos-index superior">
  <h2><?= Html::encode($this->title) ?></h2>
  <div class="g-index">
       <section class="pag-busqueda">
    <?=   ListView::widget([
            'dataProvider' => $dataProvider,
          
             'itemView' => '_libro',
              'layout'=>"\n{items}",

          ]);
  ?>
</section>
 
  <section class="sidebar-busqueda">
      <h4>Géneros</h4>
       
                    <?php foreach ($generos as $genero): ?>
                       <br>
              <?= Html::a($genero->genero, ['site/genero', 'genero' => $genero->genero]) ?>

                                       
                    <?php endforeach; ?>
  </section>
  </div>
</div>
