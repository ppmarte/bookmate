<?php

/** @var yii\web\View $this */
use yii\helpers\Html;
use yii\widgets\ListView;
use yii\grid\ActionColumn;
use yii\grid\GridView;


$this->params['breadcrumbs'][] = ['label' => 'Estanterias ', 'url' => ['site/estanteria']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);


?>
<div class="estanterias-index superior">
    <h2>
        <?= Html::encode($this->title) ?>
    </h2>
    
     <div class="estanteria-index">

            <div class="pag-estanteria">
                
              
                <?= ListView::widget([
                    'dataProvider' => $dataProvider,
                    'itemView' => '_estante',
                    'layout' => "{items}{pager}",

                ]);
                ?>
              
            </div>
            <div class="sidebar-busqueda">
                   <h5>Tus etiquetas</h5>
                  <?php foreach ($tags as $tag): ?>
                        </br><?= Html::a($tag->tag, ['site/tag', 'tag' => $tag->tag]) ?>
                        <?php endforeach; ?>
          </div>
    </div>
     </div>