
<?php
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

?>


<div class="col-sm-12">
            <div class="card minimo">
                
             <?= Html::a(Html::img('@web/img/'.$model -> id.'.jpg', ['alt'=>'Portada', 'class'=>'book cover']),['libros/view', 'id'=>$model -> id ] ) ?>
                
                 <h6 class="card-title"><?=$model -> titulo?></h6>
             
            </div>
        </div>
