<?php
use yii\helpers\Html;
use  yii\widgets\ListView;
use  yii\widgets\LinkPager;
use kartik\widgets\StarRating;
/** @var yii\web\View $this */


$this->title ='Reto de lectura '.$reto->nombre;
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="superior">
    <?php if (empty($model->objetivo)) : ?>
                  <div class="reto-actual">
 <?= Html::img('@web/img/icon/icon2.png', ['alt'=>'IconoReto', 'class'=>'bookImg'] ) ?>
        <div class="year">
            <span>Reto de Lectura 2023</span>
             <?= Html::a('Participar',['participaciones/reto' ], ['class' => 'btn btn-success']) ?>
        </div>
        <div class="challengeStats">
          
                <div class="challengeStatItem">Participantes</div>
                 <div class="statNumber"><?=$participantes?></div>
                <div class="challengeStatItem">Libros leídos</div>
                 <div class="statNumber"><?=$libros?></div>
                 <div class="challengeStatItem">Media de Libros</div>
                 <div class="statNumber"><?=$mediaLibrosPorParticipante?></div>
                 <div class="challengeStatItem">Páginas leídas</div>
                 <div class="statNumber"><?=$paginas?></div>
        </div>
    </div>
                  
                     <?php else : ?>
    <div class="reto-actual">
 <?= Html::img('@web/img/icon/icon2.png', ['alt'=>'IconoReto', 'class'=>'bookImg'] ) ?>
        <div class="year">
           Reto de Lectura 2023
            
        </div>
        <div class="challengeStats">
          
                <div class="challengeStatItem">Participantes</div>
                 <div class="statNumber"><?=$participantes?></div>
                <div class="challengeStatItem">Libros leídos</div>
                 <div class="statNumber"><?=$libros?></div>
                 <div class="challengeStatItem">Media de Libros</div>
                 <div class="statNumber"><?=$mediaLibrosPorParticipante?></div>
                 <div class="challengeStatItem">Páginas leídas</div>
                 <div class="statNumber"><?=$paginas?></div>
        </div>
    </div>
    
    <div class="info-lector">
        <h4><?= Yii::$app->user->identity->username?> has leído <?=$librosusuario?> de <?=$model->objetivo?> libros en <?=$model->codReto->nombre?></h4>
         <?= Html::a('Actualizar tu objetivo',['participaciones/reto' ], ['class' => 'btn btn-success']) ?>
    </div>
    
    <div class="libros-del-reto">
        <h3>Tus libros del <?=$model->codReto->nombre?></h3>
          <?=   ListView::widget([
            'dataProvider' => $dataProvider,
          
             'itemView' => '_libroreto',
              'layout'=>"\n{items}",

          ]);
  ?>
    </div>  
                  <?php endif; ?>
</div>