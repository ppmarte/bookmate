<?php

use yii\helpers\Html;
use yii\widgets\ListView;
use yii\widgets\LinkPager;
use kartik\widgets\StarRating;

/** @var yii\web\View $this */
$this->title = 'Bienvenido a tu perfil, ' . $model->username;
?>

<div class="site-index superior">

    <h2><?= Html::encode($this->title) ?></h2>

    <div class="perfilusuario">

        <div class="perfiluserinfo">
            <!-- Avatar por defecto en caso de que no haya una imagen del usuario -->  
            <div class="avatar">
                <?php
                $imagePath = '@web/img/user/' . $model->id . '.jpg';
                $defaultImage = '@web/img/user/icon.png';

                echo Html::img(file_exists(Yii::getAlias('@webroot/img/user/' . $model->id . '.jpg')) ? $imagePath : $defaultImage);
                ?>

            </div>

            <div class="perfilusuarioinfo">         <h3><?= $model->username ?></h3>
                <p><?= Html::a('Editar tu perfil', ['users/perfil', 'id' => $model->id]) ?></p>
                <p><?= Html::a('Desactivar cuenta', ['users/inactivar', 'id' => $model->id]) ?></p>
                <p>Has leído <span><?= $librosleidos ?></span> libros</p>
                <p>Miembro desde el  <span><?= $model->f_registro ?></span></p>
            </div>
            <?php if (!empty($model->biografia)) : ?>
                <div class="biografia"><span><?= $model->biografia ?></span></div>
            <?php endif; ?>
        </div>


    </div>


    <h3>Tus estadísticas</h3>
    <div class="perfilusuariocontainer">

        <div class="perfilinforCount"><span><?= $librosleidos ?></span>
            Libros leídos</div>

        <div class="perfilinforCount"><span><?= $paginas ?></span>
            Páginas leídas</div>

        <div class="perfilinforCount"><span><?= $retos ?></span>
            Retos en los que has participado
        </div>
    </div>



    <h3>Tus Géneros más leídos</h3>
    <div class="perfilusuariocontainer">

        <?php if (empty($generos)) : ?>
            <span>Aún no tienes un género favorito.</span>
        <?php else : ?>
            <?php foreach ($generos as $item) : ?>
                <div class="perfilinforCount">
                    <span class="count"><?= $cantidadLibros[$item['genero']]; ?> libros</span>
                    <span><?= $item['genero']; ?></span>

                </div>
            <?php endforeach; ?>
        <?php endif; ?>

    </div>
</div>