<?php

/** @var yii\web\View $this */
use yii\helpers\Html;
use yii\widgets\ListView;
use yii\grid\ActionColumn;
use yii\grid\GridView;

$this->params['breadcrumbs'][] = ['label' => 'Estanterias ', 'url' => ['site/estanteria']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="estanterias-index superior">
    <h2>
<?= Html::encode($this->title) ?>
    </h2>
        <?php
        if (Yii::$app->user->isGuest) {

            Yii::$app->getResponse()->redirect(Yii::$app->getUser()->loginUrl);
        } else {
            ?>

        <div class="estanteria-index">

            <div class="pag-estanteria">
 <!-- Vista de los libros de la estantería -->

    <?=
    ListView::widget([
        'dataProvider' => $dataProvider,
        'itemView' => '_estante',
        'layout' => "{items}{pager}",
    ]);
    ?>

            </div>
            <div class="sidebar-busqueda">
                <h5>Mis estanterías</h5>
                <br>
                 <!-- Lista con las estanterías y el número de libros en cada una -->
    <?php if ($countLibros['total'] == 0) : ?>
                    <span>Añade algún libro a tu estantería.</span>
                <?php else : ?>
                    <?= Html::a('Todos mis libros', ['site/estanteria']) ?> (
                    <?= $countLibros['total'] ?>)


        <?php foreach ($estanterias as $estanteria): ?>
                        <br>
                        <?= Html::a($estanteria->nombre, ['site/estanteriaid', 'id' => $estanteria->id]) ?>

                        <?php if (isset($cantidadLibros[$estanteria->id])): ?>
                            <span>(
                            <?= $cantidadLibros[$estanteria->id] ?> )
                            </span>
                            <?php else: ?>
                            <span>(0)</span>
                        <?php endif; ?>
                    <?php endforeach; ?>
                <?php endif; ?>
                            
                             <!-- Lista de las etiquetas personales de los usuarios de tenerlas -->
                <?php foreach ($tags as $tag): ?>
                    </br><?= Html::a($tag->tag, ['site/tag', 'tag' => $tag->tag]) ?>
                <?php endforeach; ?>

                     <!-- Zona de lecturas actuales -->
                <div class="actual">
                    <h5>Lecturas actuales</h5>
                    <br>
    <?php if (empty($librosLeyendoActualmente)) : ?>
                        <span>   No estás leyendo nada en este momento. </span>
                    <?php else : ?>
                        <?php foreach ($librosLeyendoActualmente as $libro): ?>
                            <div class="libro" data-libro-id="<?= $libro->id ?>">
                            <?= Html::img('@web/img/' . $libro->id . '.jpg', ['alt' => 'Portada', 'class' => 'leyendo book']) ?>
                                <div class="libroactualinfo">
                                <?= Html::a($libro->titulo, ['libros/view', 'id' => $libro->id], ['class' => 'd-inline-block text-truncate']) ?>
                                    <p>de
                                    <?= Html::a($libro->getCodEscritors()->one()->nombre, ['escritores/view', 'id' => $libro->getCodEscritors()->one()->id]) ?>
                                    </p>


            <?php $porcentajeLeido = $libro->porcentaje_leido($libro->agregacions[0]->paginasleidas); ?>



                                    <div class="progress" id="progresoLibro<?= $libro->id ?>">

                                        <div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar"
                                             style="width: <?= $porcentajeLeido ?>%"
                                             aria-valuenow="<?= $libro->agregacions[0]->paginasleidas ?>" aria-valuemin="0"
                                             aria-valuemax="<?= $libro->num_pag ?>">

                                        </div>

                                    </div>
                                    <div id="texto-progreso<?= $libro->id ?>">
            <?= $libro->agregacions[0]->paginasleidas ?> / <?= $libro->num_pag ?>
                                        (<?= round($libro->porcentaje_leido($libro->agregacions[0]->paginasleidas)) ?>%)
                                    </div>



                                    <!-- Botón que abre el modal -->
            <?=
            Html::button('Actualizar', [
                'class' => 'btn btn-outline-secondary btn-sm actualizar',
                'data-toggle' => 'modal',
                'data-target' => '#actualizarModal' . $libro->id,
            ])
            ?>


                                    <!-- Modal correspondiente a cada libro -->
                                    <div class="modal fade" id="actualizarModal<?= $libro->id ?>" tabindex="-1" role="dialog"
                                         aria-labelledby="actualizarModalLabel<?= $libro->id ?>" aria-hidden="true">
                                        <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title" id="actualizarModalLabel<?= $libro->id ?>">Actualizar
                                                        páginas leídas de <?= $libro->titulo ?></h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body">
                                                    <div class="form-group">
                                                        <?= Html::label('Páginas leídas') ?>
                                                        <?= Html::input('number', 'paginasLeidas', $libro->agregacions[0]->paginasleidas, ['class' => 'form-control', 'id' => 'paginasLeidas' . $libro->id]) ?>
                                                    </div>
                                                    <p>
                                                        <?= Html::a('¡Lo he terminado!', ['agregacion/actualizacion', 'id' => $libro->id]) ?>
                                                    </p>
                                                    <button class="btn-actualizar" class="btn btn-outline-secondary btn-sm"
                                                            data-toggle="modal" data-target="#actualizarModal<?= $libro->id ?>"
                                                            data-id="<?= $libro->id ?>"
                                                            data-paginas="<?= $libro->num_pag ?>">Actualizar</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        <?php endforeach; ?>
                    <?php endif; ?>
                </div>

                <div class="estanteria-reto">
                    <h5>Reto de Lectura</h5>
                    <div class="info-reto-estanteria">
                        <div class="badgeYearestanteria">
                            <span class="yearHeaderestanteria">2023</span>
                            <?= Html::img('@web/img/icon/icon2.png', ['alt' => 'IconoRetoestanteria ', 'class' => 'bookImgretoestanteria']) ?>
                            
                        </div>
                        <div class="retosestadisticas"> 
                            <?php if (empty($model->objetivo)) : ?>
                                <p>Participa en el <a href="<?= \yii\helpers\Url::to(['participaciones/reto'], true) ?>">reto</a>.</p>
                            <?php else : ?>
                                <?php if ($librosusuario >= $model->objetivo) : ?>
                                    <span>¡Reto completado!</span>
                                <?php endif; ?>
                                <p>Has leído <span><?= $librosusuario ?></span> de tu objetivo de <span><?= $model->objetivo ?></span> libros</p>

                                <div class="progress"><div class="progress-bar"  style="width: <?= $porcentaje ?>%"></div> </div>
                                <span><?= round($porcentaje)?>%</span>



                            <?php endif; ?>
                        </div> 
                    </div>
                </div>

            </div>
        </div>

    <?php } ?>
</div>