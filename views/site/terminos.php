<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\helpers\ArrayHelper;

/** @var yii\web\View $this */
/** @var app\models\Escritores $model */
$this->title = 'Terminos y condiciones';
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="escritores-view superior ">

    <div class="politica">
        <h2><?= Html::encode($this->title) ?></h2>
        <h3>¡Bienvenido a Book Mate!</h3>

        <p>
            Estos términos y condiciones describen las reglas y regulaciones para el uso
            del sitio web de Book Mate, ubicado en
            <?= Html::a('Book Mate', ['site']); ?>
        </p>

        <p>
            Al acceder a este sitio web, se asume que aceptas estos términos y
            condiciones. Si no estás de acuerdo con alguno de los términos establecidos
            en esta página, te solicitamos que no continúes utilizando Book Mate.
        </p>

        <h4>Cookies:</h4>
        <p>
            El sitio web utiliza cookies para personalizar tu experiencia en línea. Al
            acceder a Book Mate, aceptas el uso de cookies necesarias para su
            funcionamiento.
        </p>

        <p>
            Las cookies son archivos de texto colocados en tu disco duro por un servidor
            de páginas web. No se pueden utilizar para ejecutar programas o transmitir
            virus a tu computadora. Cada cookie es única y solo el servidor web que la
            emitió puede acceder a ella.
        </p>

        <p>
            Utilizamos cookies con fines estadísticos y de marketing para operar nuestro
            sitio web. Tienes la capacidad de aceptar o rechazar las cookies opcionales.
            Al aceptar las cookies requeridas, también aceptas las cookies de terceros
            que pueden utilizarse a través de servicios proporcionados por terceros
            integrados en nuestro sitio web, como ventanas de visualización de video.
        </p>

        <h4>Propiedad intelectual:</h4>
        <p>
            Book Mate y/o sus licenciantes poseen los derechos de propiedad intelectual
            de todo el material en Book Mate, a menos que se indique lo contrario. Todos
            los derechos de propiedad intelectual están reservados. Puedes acceder al
            contenido de Book Mate para uso personal, sujeto a las restricciones
            establecidas en estos términos y condiciones.
        </p>

        <h4>No debes:</h4>

        <ol>
            <li>Copiar o republicar material de Book Mate.</li>
            <li>Vender, alquilar o sublicenciar material de Book Mate.</li>
            <li>Reproducir, duplicar o copiar material de Book Mate.</li>
            <li>Redistribuir contenido de Book Mate.</li>
            <li>Este acuerdo entra en vigencia a partir de la fecha presente.</li>
        </ol>

        <h4>Comentarios de los usuarios:</h4>
        <p>
            Partes de este sitio web ofrecen a los usuarios la oportunidad de publicar e
            intercambiar opiniones e información en determinadas áreas. Book Mate no
            filtra, edita, publica ni revisa los comentarios antes de su presencia en el
            sitio web. Los comentarios reflejan las opiniones de los usuarios y no
            representan necesariamente las opiniones de Book Mate, sus agentes y/o
            afiliados.
        </p>

        <p>
            Book Mate se reserva el derecho de monitorear todos los comentarios y
            eliminar aquellos que se consideren inapropiados, ofensivos o que infrinjan
            estos términos y condiciones.
        </p>

        <h4>
            Al publicar comentarios en nuestro sitio web, garantizas y declaras que:
        </h4>
        <ol>
            <li>
                Tienes derecho a hacerlo y cuentas con todas las licencias y
                consentimientos necesarios.
            </li>
            <li>
                Tus comentarios no infringen ningún derecho de propiedad intelectual de
                terceros.
            </li>
            <li>
                Tus comentarios no contienen material difamatorio, calumnioso, ofensivo,
                indecente o ilegal, ni invaden la privacidad de otros.
            </li>
            <li>
                Tus comentarios no se utilizarán para solicitar o promover actividades
                comerciales ilícitas.
            </li>
            <li>
                Al publicar comentarios en nuestro sitio web, otorgas a Book Mate una
                licencia no exclusiva para usar, reproducir, editar y autorizar a otros a
                usar, reproducir y editar tus comentarios en cualquier forma, formato o
                medio.
            </li>
        </ol>

        <h4>Eliminación de enlaces de nuestro sitio web:</h4>
        <p>
            Si encuentras algún enlace en nuestro sitio que sea ofensivo por cualquier
            motivo, puedes contactarnos e informarnos en cualquier momento.
            Consideraremos las solicitudes para eliminar enlaces, pero no estamos
            obligados a hacerlo ni a responder directamente.
        </p>

        <p>
            No nos aseguramos de que la información de este sitio web sea correcta. No
            garantizamos su integridad o precisión, ni prometemos asegurarnos de que el
            sitio web permanezca disponible o que el material en el sitio se mantenga
            actualizado.
        </p>

        <h4>Hipervínculos:</h4>
        <p>
            Los siguientes organismos y enlaces externos pueden estar vinculados a
            nuestro sitio web:
        </p>

        <ol>
            <li>Agencias gubernamentales.</li>
            <li>Motores de búsqueda.</li>
            <li>Organizaciones de noticias.</li>
        </ol>
        <p>
            Al hacer clic en los enlaces externos, abandonas nuestro sitio web. Book
            Mate no tiene control ni asume responsabilidad por el contenido, políticas
            de privacidad o prácticas de sitios web de terceros. Es recomendable que
            leas los términos y condiciones y políticas de privacidad de cualquier sitio
            web que visites.
        </p>

        <h4>Exención de responsabilidad:</h4>
        <p>
            En la medida máxima permitida por la ley aplicable, Book Mate y sus agentes,
            empleados, socios y afiliados se eximen de cualquier responsabilidad
            derivada de tu uso o acceso a Book Mate. Esto incluye cualquier pérdida o
            daño directo, indirecto, incidental, consecuente o punitivo.
        </p>

        <h4>Modificaciones:</h4>
        <p>
            Book Mate se reserva el derecho de revisar estos términos y condiciones en
            cualquier momento. Al continuar utilizando el sitio web después de la
            notificación de cualquier cambio, aceptas los términos revisados.
        </p>
    </div>

</div>