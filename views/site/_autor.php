<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
?>


<div class="col-sm-12">
    <div class="card autores">

        <?= Html::img('@web/img/autor/' . $model->nombre . '.jpg', ['alt' => 'Autor', 'class' => 'escritoresImg']) ?>

        <?= Html::a($model->nombre, ['escritores/view', 'id' => $model->id], ['class' => 'h6']) ?>

    </div>
</div>
