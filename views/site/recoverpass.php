<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
?>
 <div class="site-login superior">
    <div class="formularioDiferente">
<h4><?= $msg ?></h4>
 
<h2>Recuperar contraseña</h2>
<?php $form = ActiveForm::begin([
    'method' => 'post',
    'enableClientValidation' => true,
]);
?>
 
<div class="form-group">
 <?= $form->field($model, 'email')->textInput(['email','placeholder' => 'Introduce tu email']) ?>  
</div>
 
<?= Html::submitButton('Recuperar contraseña', ['class' => 'btn btn-primary']) ?>
 
<?php $form->end() ?>
</div>
 
     </div>
 