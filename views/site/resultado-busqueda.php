<?php

/** @var yii\web\View $this */
use yii\helpers\Html;
use  yii\widgets\ListView;
$progress=70;
//$user_id = Yii::$app->user->identity->id;
$this->title = 'Búsqueda';
$this->params['breadcrumbs'][] = $this->title;
?>
  

    
  <div class="text-center superior">
       <?php if (Yii::$app->user->isGuest) {

        Yii::$app->getResponse()->redirect(Yii::$app->getUser()->loginUrl);

    } else { ?>
 <h2>Resultados de la búsqueda para "<?= Html::encode($busqueda) ?>"</h2>
 
 <div class="resultado-busqueda ">
    <section class="pag-busqueda">



<?php foreach ($registros as $registro): ?>
    <div class="col-sm-3">
        <div class="card minimo">
            <?= Html::a(Html::img('@web/img/'.$registro -> id.'.jpg', ['alt'=>'Portada', 'class'=>'book cover']),['libros/view', 'id'=>$registro-> id ] ) ?>

            <h6><?= Html::a($registro->titulo, ['libros/view', 'id'=>$registro-> id ] ) ?> </h6>
            
            <?php if (!empty($registro->codEscritors)): ?>
                <?php $escritor = $registro->codEscritors[0]; // Asigna el primer escritor encontrado a la variable $escritor ?>
                <?= Html::a($escritor->nombre, ['escritores/view', 'id' => $escritor->id]) ?>
            <?php else: ?>
                No se encontró el escritor
            <?php endif; ?>
        </div>
    </div>
<?php endforeach; ?>
   </section>

  <section class="sidebar-busqueda">
      <h4>Géneros</h4>
       <?php foreach ($generos as $genero): ?>
         <br>
              <?= Html::a($genero->genero, ['site/genero', 'genero' => $genero->genero]) ?>
        <?php endforeach; ?>
     
 </div>
 <?php } ?>
 
</div>

