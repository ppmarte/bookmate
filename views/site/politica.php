<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\helpers\ArrayHelper;

/** @var yii\web\View $this */
/** @var app\models\Escritores $model */
$this->title = 'Política de Privacidad';
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="escritores-view superior ">

    <div class="politica">
        <h2><?= Html::encode($this->title) ?></h2>

        <p>
            En esta página puedes encontrar información sobre cómo se manejarán y
            protegerán tus datos personales. La política de privacidad expresa cómo Book
            Mate tratará la información personal de todas las personas que interactúan
            con el sitio web. Te pedimos que leas cuidadosamente esta política de
            privacidad antes de utilizar la web, la cual está redactada según las
            últimas directrices europeas, de forma que no necesites de un abogado para
            comprenderlas sino que simplemente leyendo cualquier persona esta política
            pueda ser capaz de comprenderla totalmente.
        </p>

        <p>
            De acuerdo con el Reglamento UE 2016/679 y la Ley Orgánica 3/2018, Book Mate
            te informa que al aceptar esta política de privacidad, prestas tu
            consentimiento expreso, informado, libre e inequívoco para que Book Mate
            procese tus datos personales, los cuales serán protegidos con medidas
            técnicas y organizativas adecuadas a la normativa vigente.
        </p>

        <h4>Datos personales tratados</h4>
        <p>
            En este sitio web se pueden recoger los siguientes datos, siempre para poder
            ofrecer el servicio que la web tiene como finalidad, dependiendo de las
            acciones realizadas en el sitio web por el usuario:
        </p>

        <p>
            Datos de navegación: el sitio web puede conocer tu dirección IP, dispositivo
            utilizado, navegador con el que accede el usuario al sitio web y los datos
            de navegación y comportamiento del usuario dentro del sitio. Esto es para
            poder ofrecer la mejor experiencia de uso a los usuarios del sitio web.
        </p>

        <p>
            Datos identificativos y de contacto: nombre, apellidos, correo electrónico o
            cualquier otro método de contacto siempre que el usuario voluntariamente los
            indique.
        </p>

        <p>
            Datos contractuales: en caso de adquirir bienes o servicios ofrecidos en el
            sitio web.
        </p>
        <h4>Base legal del tratamiento de datos</h4>
        <p>
            Los datos de los usuarios son tratados en base a las siguientes bases
            legales:
        </p>
        <p>
            Interés legítimo del responsable del tratamiento de datos para proteger a
            los usuarios de cualquier tipo de fraude o abuso en el uso de los servicios.
        </p>
        <p>
            Consentimiento otorgado por el usuario relativo a contacto, suscripciones o
            comunicaciones comerciales.
        </p>
        <p>
            Ejecución de contratos con Book Mate para contratar servicios y poder
            gestionar la entrega de los mismos.
        </p>

        <h4>Tiempo de conservación de los datos personales</h4>

        <p>
            El tiempo de conservación de los datos personales de los usuarios será el
            menor posible en todo caso, aunque pudiendo mantenerse en más tiempo en caso
            de tener que cumplir con algún imperativo legal que obligue a dicho
            mantenimiento, como es el caso de las infracciones y sanciones en el orden
            social (4 años), las acciones civiles personales (5 años), a efectos
            contables (6 años) y para cumplir con algunas normativas relativas a la
            Prevención de Blanqueo de Capitales y Financiación de terrorismo (10 años).
            En cualquier caso, en términos generales y sin que concurra ninguna de las
            circunstancias que obliguen a un mantenimiento mayor de los datos, se
            tendrán el tiempo mínimo posible.
        </p>

        <h4>¿Qué derechos tienes respecto de tus datos?</h4>
        <p>
            Tus datos son tuyos, por lo que tendrás todo el derecho a conocer el
            tratamiento de datos que se les da por parte de Book Mate a tus datos. En
            concreto, tendrás derecho a:
        </p>
        <ul>
            <li>Acceder a tus datos</li>
            <li>Rectificar o suprimir los datos</li>
            <li>Cancelación de los datos</li>
            <li>Limitación del tratamiento de los datos</li>
            <li>Oposición al tratamiento</li>
            <li>Portabilidad de los datos en cualquier momento</li>
        </ul>

        <p>
            Además, tienes derecho a retirar el consentimiento en cualquier momento
            acerca del tratamiento de tus datos, aunque hayas consentido el mismo para
            una finalidad concreta dentro de la web. En caso de que consideres que el
            tratamiento de tus datos vulnera tus derechos, puedes ponerte en contacto
            con la Agencia de Protección de Datos a través del siguiente enlace:
            https://www.aepd.es/
        </p>

        <p>
            Si quieres ejercitar alguno de los derechos anteriormente descritos, puedes
            hacerlo en cualquier momento enviando un correo electrónico a la dirección
            bookmate@bookmate.com. Para ello necesitarás enviar el formulario de
            ejercicio de derechos sobre datos personales, que podrás solicitarnos vía
            email o descargar de la propia web de la Agencia de Protección de Datos. En
            cualquier caso, deberán ir firmados electrónicamente o adjuntando una
            fotocopia/imagen de tu DNI o documento identificativo correspondiente.
        </p>

        <p>
            El formulario podrá enviarse tanto por correo electrónico como vía correo
            ordinario a la dirección Calle de la Luz 5, 2 C, Santander, Cantabria.
        </p>

        <h4>Seguridad y declaración de brechas de seguridad</h4>

        <p>
            En
            <?= Html::a('Book Mate', ['site']); ?>
            tomamos todas las medidas a nuestro alcance y adecuadas al nivel de riesgo
            de los datos manejados para proteger tu información personal. En caso de que
            detectemos alguna brecha de seguridad o creamos razonablemente que alguno de
            tus datos puede haber sido expuesto, nos pondremos en contacto de forma
            inmediata para que puedas tomar las medidas de protección necesarias frente
            a cualquier tipo de ataque dirigido contra tus datos personales.
        </p>

        <h4>Seguridad y mantenimiento de tus datos</h4>
        <p>
            En
            <?= Html::a('Book Mate', ['site']); ?>
            nos comprometemos a utilizar y tratar tus datos personales con total
            confidencialidad, de manera acorde a la finalidad de los mismos, así como
            adaptar todas las medidas para evitar cualquier tipo de alteración o pérdida
            de los mismos, así como accesos no autorizados, en base a lo establecido por
            la normativa vigente en materia de protección de datos.
        </p>

        <p>
            Por otro lado,<?= Html::a('Book Mate', ['site']); ?>
            debe ser claro y transparente con el hecho de que, al igual que cualquier
            otro sitio web de internet, no podemos garantizar la seguridad y el no
            acceso autorizado por parte de piratas informáticos o cualquier otro
            individuo u organización criminal, lo cual podría violar la confidencialidad
            de los datos custodiados.
        </p>
        <p>
            Respecto del manejo de datos por otro tipo de procesamiento,
            <?= Html::a('Book Mate', ['site']); ?>
            se encargará siempre de que el procesamiento realizado por cualquier persona
            autorizada por Book Mate estará bajo la misma obligación de confidencialidad
            que nosotros.
        </p>
        <p>
            En cualquier caso, si en algún momento hay algún error o fuga de información
            o datos personales, Book Mate lo comunicará a la mayor brevedad posible a
            los usuarios afectados o potencialmente afectados por ello.
        </p>

        <h4>Exactitud y veracidad de los datos</h4>
        <p>
            Al ser quien los proporciona, el único responsable de la veracidad de los
            datos proporcionados a
            <?= Html::a('Book Mate', ['site']); ?>eres tú como usuario, no teniendo Book
            Mate ninguna responsabilidad respecto a la exactitud o veracidad de los
            datos. En cualquier caso, los usuarios deben proporcionar datos exactos y
            auténticos, manteniendo los mismos debidamente actualizados.
        </p>
        <p>
            El usuario se compromete a proporcionar información correcta y completa en
            base a esta política de privacidad siempre que remita datos de cualquier
            tipo Book Mate a través de http://localhost/bookmate/web/.
        </p>

        <h4>Revocabilidad de los datos</h4>
        <p>
            El usuario presta su consentimiento para el tratamiento y cesión de los
            datos en los términos expuestos, pero este no es un derecho absoluto, dado
            que puede ser revocado por el titular de los datos en cualquier momento, con
            la mera manifestación de su voluntad y llevando a cabo el procedimiento
            descrito anteriormente para el ejercicio de los derechos arco ante Book
            Mate. En cualquier caso, esta revocación no tendrá carácter retroactivo en
            ningún caso.
        </p>

        <h4>Cambios en la política de privacidad</h4>
        <p>
            Book Mate tiene reservado el derecho a modificar la presente política de
            privacidad para adaptarla a las novedades legislativas que existan en el
            futuro, así como a las propias prácticas comunes de la industria o
            particulares de Book Mate. En caso de actualización de las políticas, se
            anunciarán en esta página cualesquiera cambios con anterioridad a la
            aplicación y obligatoriedad de los mismos.
        </p>
    </div>

</div>