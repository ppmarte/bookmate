
<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\editors\Summernote;

$this->title = 'Añadir un comentario a ';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="superior">
    <div class="formularioDiferente">
        <h3><?= Html::encode($this->title) ?></h3>
        <!-- Formulario para añadir un nuevo comentario -->
        <?php $form = ActiveForm::begin(); ?>


        <?= $form->field($agregacion->codLibro, 'titulo')->textInput(['readonly' => true]) ?>

        <?=
        $form->field($model, 'comentario')->widget(Summernote::class, [
            'useKrajeePresets' => true,
            'pluginOptions' => [
                'height' => 200,
                'dialogsFade' => true,
                'toolbar' => [
                    ['style1', ['style']],
                    ['style2', ['bold', 'italic', 'underline', 'strikethrough', 'superscript', 'subscript']],
                    ['font', ['fontname', 'fontsize', 'color', 'clear']],
                    ['para', ['ul', 'ol', 'paragraph', 'height']],
                    ['insert', ['link', 'picture', 'video', 'table', 'hr']],
                ],
                'fontSizes' => ['8', '9', '10', '11', '12', '13', '14', '16', '18', '20', '24', '36', '48'],
                'codemirror' => [
                    //  'theme' => Codemirror::DEFAULT_THEME,
                    'lineNumbers' => true,
                    'styleActiveLine' => true,
                    'matchBrackets' => true,
                    'smartIndent' => true,
                    'enablePrettyFormat' => false,
                    'autoFormatCode' => false,
                ],
                'placeholder' => 'Escribe tu comentario',
            ]
        ]);
        ?>


        <?= Html::submitButton('Añadir a estantería', ['class' => 'btn btn-primary']) ?>

        <?php ActiveForm::end(); ?>
    </div>


</div>