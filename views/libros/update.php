<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Libros $model */

$this->title = 'Modificar ' . $model->titulo;
$this->params['breadcrumbs'][] = ['label' => 'Libros', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->titulo, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Modificar';
?>
<div class="libros-update administradores">

    <h2><?= Html::encode($this->title) ?></h2>

     <?= $this->render('_form', [
        'model' => $model,
         'autorActual' => $autorActual,
          'generos' => $generos,
         'generosSeleccionados' => $generosSeleccionados,
        'generosActuales' => $generosActuales, // Pasar los géneros actuales a la vista
        // 'escritoresOptions'=>$escritoresOptions,
      //   'autores'=>$autores,
    ]) ?>

</div>
