<?php

use yii\widgets\ActiveForm;
use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\helpers\ArrayHelper;
use kartik\widgets\StarRating;
use yii\helpers\Url;

/** @var yii\web\View $this */
/** @var app\models\Libros $model */
$this->title = $model->titulo;
$this->params['breadcrumbs'][] = ['label' => 'Libros', 'url' => ['site/libros']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="libros-view superior-view">
    <?php
    if (Yii::$app->user->isGuest) {
        Yii::$app->getResponse()->redirect(Yii::$app->getUser()->loginUrl);
    } else {
        ?>

        <div class="libros-lateral"> 
            <div class="info-portada-libro" >  
                <?= Html::img('@web/img/' . $model->id . '.jpg', ['alt' => 'Portada', 'class' => 'libro detail ']) ?>
                <!-- Opción de calificar un libro -->
                <p>
                    <?=
                    StarRating::widget([
                        'name' => 'input-4',
                        'value' => $model->getCalificacionUsuario(), // Asigna el valor de la calificación del modelo aquí
                        'pluginOptions' => [
                            'showClear' => false,
                            'showCaption' => false,
                            'step' => 0.5,
                            'size' => 'md',
                        ],
                        'pluginEvents' => [
                            'rating:change' => 'function(event, value, caption) {
                                            $.ajax({
                                                url: "' . Url::to(['libros/actualizarcalificacion', 'id' => $model->id]) . '",
                                                method: "POST",
                                                data: {
                                                calificacion: value, 
                                                 },
                                                success: function(response) {
                                                    console.log("Calificación actualizada exitosamente");
                                                },
                                                error: function(xhr, status, error) {
                                                    console.error("Error al actualizar la calificación:", error);
                                                }
                                            });
                                        }'
                        ]
                    ]);
                    ?>
                </p>
                <!-- Botón para añadir a una estantería -->
                <?= Html::a('Añade a una estantería', ['agregacion/add-to-shelf', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>

            </div> 
        </div>
        <div class="info-libro">
            <h2><?= $model->titulo ?></h2>
            <div class="ranking">

                <?=
                StarRating::widget([
                    'name' => 'rating_33',
                    'value' => $model->calificacionMedia,
                    'pluginOptions' => [
                        'size' => 'sm',
                        'readonly' => true,
                        'showClear' => false,
                        'showCaption' => true,
                        'language' => 'es',
                    ],
                ]);
                ?> 
                <?php if ($model->calificacionMedia > 0) : ?>
                    <span class="numcalificacion"><?= $model->numCalificaciones ?> <?= $model->numCalificaciones == 1 ? 'calificación' : 'calificaciones' ?>
                    </span>   <?php endif; ?>
            </div>
            <!-- Escritores de un libro -->
            <div class="autores">
                <?php
                $escritores = $model->codEscritors;
                foreach ($escritores as $escritor) {
                    $enlace = Yii::$app->urlManager->createUrl(['escritores/view', 'id' => $escritor->id]);
                    echo '<a href="' . $enlace . '">' . $escritor->nombre . '</a><br>';
                }
                ?>
            </div>
            <!-- Géneros de un libro -->
            <div class="genero" >
                <?php
                $generos = $model->generos;
                foreach ($generos as $genero) {
                    $enlace = Yii::$app->urlManager->createUrl(['site/genero', 'genero' => $genero->genero]);
                    echo '<a href="' . $enlace . '">' . $genero->genero . '</a>   <br>';
                }
                ?>
            </div>  


            <div class="sipnosis">  
                <?= $model->sipnosis ?>
            </div>  




            <!-- Comentarios de los libros -->
            <div class="comentarios-libros">
                <h3>Calificaciones y reseñas</h3>

                <?= Html::a('Añade un comentario', ['comentarios/comentario', 'codlibro' => $model->id], ['class' => 'btn btn-primary']) ?>
                <p> </p>


                <?php $hasComments = false; ?>
                <?php foreach ($model->agregacions as $agregacion) : ?>
                    <?php if (empty($agregacion->comentarios)) : ?>
                        <?php continue; ?>
                    <?php else : ?>
                        <?php $hasComments = true; ?>
                        <?php foreach ($agregacion->comentarios as $comentario) : ?>
                            <div class="comentarios">
                                <div class="perfil">
                                    <?= Html::img('@web/img/user/' . $agregacion->codUser->id . '.jpg') ?>
                                    <p><?= $agregacion->codUser->username ?></p>

                                </div>
                                <div class="comentario">
                                    <span><?= $agregacion->f_agregado ?></span>
                                    <?= $comentario->comentario ?></div>
                            </div>
                        <?php endforeach; ?>
                    <?php endif; ?>
                <?php endforeach; ?>
                <!--En caso de que no haya ningún comentario -->
                <?php if (!$hasComments) : ?>
                    <p>Aún no hay comentarios.</p>
                <?php endif; ?>


            </div>
        </div>
    <?php } ?>
</div>



