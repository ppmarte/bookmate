<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\editors\Summernote;
use kartik\date\DatePicker;
use kartik\widgets\StarRating;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use app\models\Escritores;
use app\models\Generos;
/** @var yii\web\View $this */
/** @var app\models\Libros $model */
/** @var yii\widgets\ActiveForm $form */


?>

<div class="libros-form">

    <?php $form = ActiveForm::begin();
?>
<div class="form-group">
    <?= $form->field($model, 'ISBN')->textInput(['maxlength' => true,'placeholder' => 'Escribe el ISBN del libro']) ?>
</div>
<div class="form-group">
    <?= $form->field($model, 'titulo')->textInput(['maxlength' => true,'placeholder' => 'Escribe el título del libro']) ?>
</div>
<div class="form-group">
<?= $form->field($model, 'cod_escritor')->label('Escritor')->dropDownList(Escritores::getEscritoresOptions(), ['value' => isset($autorActual) ? $autorActual : null]) ?>
<?= isset($autorActual) ? Html::hiddenInput('autorActual', $autorActual) : '' ?>
 </div>
    <div class="form-group">
<?php $generosSeleccionados = isset($generosSeleccionados) ? $generosSeleccionados : []; ?>

<?php foreach ($generos as $key => $genero) : ?>
    <div class="form-check">
        <?= Html::checkbox("Libros[generos][]", isset($genero['checked']) && $genero['checked'], ['label' => isset($genero['label']) ? $genero['label'] : $genero, 'value' => $key]) ?>
    </div>
<?php endforeach; ?>

    </div>
<div class="form-group">
    <?= $form->field($model, 'num_pag')->textInput(['placeholder' => 'Escribe el número de páginas del libro']) ?>
</div>
<div class="form-group">
    <?= $form->field($model, 'f_publi') ->widget(DatePicker::classname(), [
  'name'=>'f_publi',
    'value'=>'2015-10-19',
        'options' => ['placeholder' => 'Seleccione una fecha'],
   //'useWithAddon'=>true,
    'convertFormat'=>true,
    'pluginOptions'=>[
         'todayHighlight' => true,
         'todayBtn' => true,
       'autoclose' => true,
        'format' => 'dd-M-yyyy',
    ]
]) 
            ?>
</div>

<div class="form-group">   
    <?= $form->field($model, 'sipnosis')->widget(Summernote::class, [
 'useKrajeePresets' => true,
        'pluginOptions'=>[
    'height' => 200,
    'dialogsFade' => true,
    'toolbar' => [
        ['style1', ['style']],
        ['style2', ['bold', 'italic', 'underline', 'strikethrough', 'superscript', 'subscript']],
        ['font', ['fontname', 'fontsize', 'color', 'clear']],
        ['para', ['ul', 'ol', 'paragraph', 'height']],
        ['insert', ['link', 'picture', 'video', 'table', 'hr']],
    ],
    'fontSizes' => ['8', '9', '10', '11', '12', '13', '14', '16', '18', '20', '24', '36', '48'],
    'codemirror' => [
      //  'theme' => Codemirror::DEFAULT_THEME,
        'lineNumbers' => true,
        'styleActiveLine' => true,
        'matchBrackets' => true,
        'smartIndent' => true,
        'enablePrettyFormat'=>false,
        'autoFormatCode'=>false,
        
    ],
             'placeholder' => 'Escribe la sipnosis del libro',
]

]);?>
   
</div>

     <div class="form-group">
         <?= Html::resetButton('Borrar', ['class' => 'btn btn-secondary btn-default']) ?>
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
