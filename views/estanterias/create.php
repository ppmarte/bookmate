<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Estanterias $model */

$this->title = 'Nueva Estanteria';
$this->params['breadcrumbs'][] = ['label' => 'Estanterias', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="estanterias-create administradores">

    <h2><?= Html::encode($this->title) ?></h2>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
