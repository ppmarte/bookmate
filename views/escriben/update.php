<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Escriben $model */

$this->title = 'Modificar Escriben: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Escribens', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Modificar';
?>
<div class="escriben-update administradores">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
