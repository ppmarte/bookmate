<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Escriben $model */

$this->title = 'Nuevo Escriben';
$this->params['breadcrumbs'][] = ['label' => 'Escribens', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="escriben-create administradores">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
