<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var app\models\Escriben $model */
/** @var yii\widgets\ActiveForm $form */
?>

<div class="escriben-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'cod_escritor')->textInput() ?>

    <?= $form->field($model, 'cod_libro')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
