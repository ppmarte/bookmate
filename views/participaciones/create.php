<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Participaciones $model */

$this->title = 'Create Participaciones';
$this->params['breadcrumbs'][] = ['label' => 'Participaciones', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="participaciones-create administradores">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
