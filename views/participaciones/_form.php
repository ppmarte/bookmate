<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var app\models\Participaciones $model */
/** @var yii\widgets\ActiveForm $form */
?>

<div class="participaciones-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'cod_user')->textInput() ?>

    <?= $form->field($model, 'cod_reto')->textInput() ?>

    <?= $form->field($model, 'objetivo')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
