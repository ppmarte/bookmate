<?php

/** @var yii\web\View $this */
use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = 'Reto de lectura ' . $reto->nombre;
$this->params['breadcrumbs'][] = $this->title;
?>
<!-- Información y números del reto actual -->
<div class="reto-index superior">
    <h2><?= Html::encode($this->title) ?></h2>
    <div class="reto-contendor">   
        <div class="badgeYear">
            <span class="yearHeader">2023</span>
            <?= Html::img('@web/img/icon/icon2.png', ['alt' => 'IconoReto', 'class' => 'bookImgreto']) ?>
            <div class="challengeText" >
                <div class="challengeText--reading" >RETO DE LECTURA</div>

            </div>


        </div>  <div class="reto-contendor-text"><div class="challengeText--reading" >¿Cuántos libros planeas leer este año?</div>
            <?php $form = ActiveForm::begin(); ?>
            <!-- FOrmulario para poner/modificar el objetivo del reto -->
            <?= $form->field($model, 'objetivo')->textInput() ?>

            <div class="form-group">
                <?= Html::submitButton('Participar', ['class' => 'btn btn-primary']) ?>
            </div>

            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>