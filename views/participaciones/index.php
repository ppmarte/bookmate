<?php

use app\models\Participaciones;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/** @var yii\web\View $this */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = 'Participaciones';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="participaciones-index admintabla">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Participaciones', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [

            'cod_user',
            'cod_reto',
            'objetivo',
            'completado',
            [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, Participaciones $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'id' => $model->id]);
                 }
            ],
        ],
    ]); ?>


</div>
