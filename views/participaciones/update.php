<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Participaciones $model */

$this->title = 'Update Participaciones: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Participaciones', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Modificar';
?>
<div class="participaciones-update administradores">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
