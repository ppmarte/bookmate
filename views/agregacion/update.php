<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Agregacion $model */

$this->title = 'Update Agregacion: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Agregacions', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Modificar';
?>
<div class="agregacion-update administradores">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
