<?php

use app\models\Agregacion;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/** @var yii\web\View $this */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = 'Agregados';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="agregacion-index admintabla">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Nueva Agregación', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
          
            'cod_libro',
            'cod_user',
            'cod_estanterias',
            'f_agregado',
            'paginasleidas',
            'calificacion',
            [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, Agregacion $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'id' => $model->id]);
                 }
            ],
        ],
    ]); ?>


</div>
