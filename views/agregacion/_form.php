<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;
/** @var yii\web\View $this */
/** @var app\models\Agregacion $model */
/** @var yii\widgets\ActiveForm $form */
?>

<div class="agregacion-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'cod_libro')->textInput() ?>

    <?= $form->field($model, 'cod_user')->textInput() ?>

    <?= $form->field($model, 'cod_estanterias')->textInput() ?>
    
    
<?= $form->field($model, 'f_agregado', [
   'labelOptions' => ['label' => 'Fecha de agregación']
])->widget(DatePicker::classname(), [
    'name'=>'fecha de agregacion',
    'convertFormat'=>true,
    'disabled' => true,
    'options' => [
        'value' => date('d-m-Y'),
    ]
]) ?>
    

    <?= $form->field($model, 'calificacion')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
