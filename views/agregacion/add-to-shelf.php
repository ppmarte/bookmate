
<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

$this->title = 'Añadir ' . $agregacion->codLibro->titulo . ' a estantería';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="superior">
    <div class="formularioDiferente">
        <h3><?= Html::encode($this->title) ?></h3>
        <!-- Formulario para añadir un libro a una estantería -->
        <?php $form = ActiveForm::begin(); ?>


        <?= $form->field($agregacion->codLibro, 'titulo')->textInput(['readonly' => true]) ?>

        <?= $form->field($agregacion, 'cod_estanterias')->dropDownList(ArrayHelper::map($estanterias, 'id', 'nombre'), ['prompt' => 'Seleccione una estantería']) ?>

        <?= $form->field($tags, 'tag')->label('Añade una etiqueta')->textInput(['maxlength' => true, 'placeholder' => 'Añade una Tag para el libro']) ?>


        <?= Html::submitButton('Añadir a estantería', ['class' => 'btn btn-primary']) ?>

        <?php ActiveForm::end(); ?>
    </div>


</div>

