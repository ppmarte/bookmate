<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Agregacion $model */

$this->title = 'Create Agregacion';
$this->params['breadcrumbs'][] = ['label' => 'Agregacions', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="agregacion-create administradores">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
