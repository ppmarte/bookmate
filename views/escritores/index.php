<?php

use app\models\Escritores;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/** @var yii\web\View $this */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = 'Escritores';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="escritores-index admintabla">

    <h2><?= Html::encode($this->title) ?></h2>

    <p>
        <?= Html::a('Nuevo Escritor', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            'nombre',
            'f_nacimiento',
            'nacionalidad',
                [
            'attribute' => 'biografia',
            'format' => 'raw',
        ],
            [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, Escritores $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'id' => $model->id]);
                 }
            ],
        ],
    ]); ?>


</div>
