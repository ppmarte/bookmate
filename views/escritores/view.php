<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\helpers\ArrayHelper;

/** @var yii\web\View $this */
/** @var app\models\Escritores $model */
$this->title = $model->nombre;
$this->params['breadcrumbs'][] = ['label' => 'Escritores', 'url' => ['site/escritores']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="escritores-view superior admintabla">

    <div class="vista-escritores">
        <div class="info-escritores">
            <div class="info-escritores-img">
                <?= Html::img('@web/img/autor/' . $model->nombre . '.jpg', ['alt' => 'Autor', 'class' => 'escritoresImg']) ?>

            </div>
            <div class="info-escritores-bio">
                <h2> <?= Html::encode($this->title) ?> </h2>
                <?php if (!empty($model->f_nacimiento)) : ?>
                    <p><span>Fecha de nacimiento: </span><?= $model->f_nacimiento ?></p>
                <?php endif; ?>
                <?php if (!empty($model->nacionalidad)) : ?>
                    <p><span>Nacionalidad: </span><?= $model->nacionalidad ?></p>
                <?php endif; ?>
                <?php if (!empty($model->biografia)) : ?>
                    <p><span>Sobre <?= $model->nombre ?></span></p>

                    <p><?= $model->biografia ?></p>
                <?php endif; ?>
            </div>
        </div>
        <!-- Zona donde se muestran los libros del autor -->
        <div class="libro-escritores">
            <h3>Sus libros:</h3>
            <section class="libros-autor">  
                <?php foreach ($model->getCodLibros()->all() as $libro): ?>

                    <div class="card minimo">
                        <?= Html::a(Html::img('@web/img/' . $libro->id . '.jpg', ['alt' => 'Portada', 'class' => 'book escritos']), ['libros/view', 'id' => $libro->id]) ?>
                    </div> 

                <?php endforeach; ?>


            </section>
        </div>
    </div> 


</div>
