<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\editors\Summernote;
use kartik\date\DatePicker;

/** @var yii\web\View $this */
/** @var app\models\Escritores $model */
/** @var yii\widgets\ActiveForm $form */
?>

<div class="escritores-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'nombre')->textInput(['maxlength' => true,'placeholder' => 'Escribe el nombre del escritor']) ?>

    <?= $form->field($model, 'f_nacimiento')->widget(DatePicker::classname(), [
  'name'=>'f_publi',
    'value'=>'2015-10-19',
        'options' => ['placeholder' => 'Seleccione una fecha'],
   //'useWithAddon'=>true,
    'convertFormat'=>true,
    'pluginOptions'=>[
         'todayHighlight' => true,
         'todayBtn' => true,
       'autoclose' => true,
        'format' => 'dd-M-yyyy',
    ]
]) 
            ?>

    <?= $form->field($model, 'nacionalidad')->textInput(['maxlength' => true, 'placeholder' => 'Escribe la nacionalidad']) ?>

    <?= $form->field($model, 'biografia')->widget(Summernote::class, [
 'useKrajeePresets' => true,
        'pluginOptions'=>[
    'height' => 200,
    'dialogsFade' => true,
    'toolbar' => [
        ['style1', ['style']],
        ['style2', ['bold', 'italic', 'underline', 'strikethrough', 'superscript', 'subscript']],
        ['font', ['fontname', 'fontsize', 'color', 'clear']],
        ['para', ['ul', 'ol', 'paragraph', 'height']],
        ['insert', ['link', 'picture', 'video', 'table', 'hr']],
    ],
    'fontSizes' => ['8', '9', '10', '11', '12', '13', '14', '16', '18', '20', '24', '36', '48'],
    'codemirror' => [
      //  'theme' => Codemirror::DEFAULT_THEME,
        'lineNumbers' => true,
        'styleActiveLine' => true,
        'matchBrackets' => true,
        'smartIndent' => true,
        'enablePrettyFormat'=>false,
        'autoFormatCode'=>false,
        
    ],
             'placeholder' => 'Escribe la biografía del escritor',
]

]);?>

    <div class="form-group">
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
