<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Escritores $model */

$this->title = 'Nuevo Escritor';
$this->params['breadcrumbs'][] = ['label' => 'Escritores', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="escritores-create administradores">

    <h2><?= Html::encode($this->title) ?></h2>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
