<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Escritores $model */

$this->title = 'Modificar ' . $model->nombre;
$this->params['breadcrumbs'][] = ['label' => 'Escritores', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->nombre, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Modificar';
?>
<div class="escritores-update administradores">

    <h2><?= Html::encode($this->title) ?></h2>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
