<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Retos $model */

$this->title = 'Nuevo Reto';
$this->params['breadcrumbs'][] = ['label' => 'Retos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="retos-create administradores">

    <h2><?= Html::encode($this->title) ?></h2>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
