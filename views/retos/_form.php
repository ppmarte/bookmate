<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;
/** @var yii\web\View $this */
/** @var app\models\Retos $model */
/** @var yii\widgets\ActiveForm $form */
?>

<div class="retos-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'nombre')->textInput(['maxlength' => true,'placeholder' => 'Escribe el año del reto']) ?>

    <div class="form-group">
    <?= $form->field($model, 'f_inicio') ->widget(DatePicker::classname(), [
  'name'=>'f_publi',
    'value'=>'2015-10-19',
        'options' => ['placeholder' => 'Seleccione una fecha'],
   //'useWithAddon'=>true,
    'convertFormat'=>true,
    'pluginOptions'=>[
         'todayHighlight' => true,
         'todayBtn' => true,
       'autoclose' => true,
        'format' => 'dd-M-yyyy',
    ]
]) 
            ?>
</div>
    
    <div class="form-group">
    <?= $form->field($model, 'f_fin') ->widget(DatePicker::classname(), [
  'name'=>'f_publi',
    'value'=>'2015-10-19',
        'options' => ['placeholder' => 'Seleccione una fecha'],
   //'useWithAddon'=>true,
    'convertFormat'=>true,
    'pluginOptions'=>[
         'todayHighlight' => true,
         'todayBtn' => true,
       'autoclose' => true,
        'format' => 'dd-M-yyyy',
    ]
]) 
            ?>
</div>


    <div class="form-group">
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
