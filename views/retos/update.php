<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Retos $model */

$this->title = 'Modificar reto: ' . $model->nombre;
$this->params['breadcrumbs'][] = ['label' => 'Retos', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->nombre, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Modificar';
?>
<div class="retos-update administradores">

    <h2><?= Html::encode($this->title) ?></h2>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
