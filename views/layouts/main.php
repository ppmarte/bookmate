<?php

/** @var string $content */
use app\assets\AppAsset;
use app\widgets\Alert;
use yii\bootstrap4\Breadcrumbs;
use yii\bootstrap4\Html;
use yii\bootstrap4\Nav;
use yii\bootstrap4\NavBar;
use app\assets\FontAsset;
use yii\helpers\Url;

$baseUrl = \Yii::$app->request->baseUrl;

define('BUSQUEDA_AJAX_URL', Url::to(['controller/actionBusquedaajax']));

AppAsset::register($this);
FontAsset::register($this);

$this->registerJsFile(
        '@web/js/main.js',
        ['depends' => [\yii\web\JqueryAsset::className()]]
);
?>
<script>
    var baseUrl = '<?php echo Yii::$app->request->baseUrl ?>';
</script>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>" class="h-100">
    <head>
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.10.3/font/bootstrap-icons.css">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css">
        <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
        <link rel="shortcut icon" href="<?php echo Yii::$app->request->baseUrl; ?>/bm.png" type="image/x-icon" />
        <meta charset="<?= Yii::$app->charset ?>">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<?php $this->registerCsrfMetaTags() ?>
        <title><?= Html::encode($this->title) ?></title>
        <?php $this->head() ?>
    </head>
    <body class="d-flex flex-column h-100" >
        <?php $this->beginBody() ?>

        <?php
        NavBar::begin([
            'brandLabel' => Yii::$app->name,
            'brandUrl' => Yii::$app->homeUrl,
            'options' => [
                'class' => 'navbar navbar-expand-md navbar-dark bg-dark  fixed-top',
            ],
        ]);

        // Definir los items de la barra de navegación en una variable
        $items = [
            '<form method="GET">
            <li>
                <span class="buscar">
                    <input type="text" id="buscar" placeholder="Buscar" onkeydown="if (event.keyCode == 13) { buscar_ahora(); return false; }" autocomplete="off" onkeyup="buscar_en_tiempo_real();" oninput="limpiar_resultados();"/>
                    <div id="resultados-busqueda"></div>
                    <div class="btnsearch" onclick="buscar_ahora();">
                        <i class="fas fa-search icon"></i>
                    </div>
                </span>
            </li>
        </form>',
            ['label' => 'Inicio', 'url' => ['/site/index']],
        ];

        // Agregar los items de inicio de sesión y cierre de sesión si el usuario está conectado
        if (Yii::$app->user->isGuest) {
            $items[] = ['label' => 'Conectarse', 'url' => ['/site/login']];
            $items[] = ['label' => 'Registrarse', 'url' => ['/site/register']];
        } else {
            $items[] = ['label' => 'Libros', 'url' => ['/site/libros']];
            $items[] = ['label' => 'Escritores', 'url' => ['/site/escritores']];
            $items[] = [
                'label' => Yii::$app->user->identity->username,
                'items' => [
                    ['label' => 'Mi perfil', 'url' => ['/site/perfil']],
                    ['label' => 'Mis libros', 'url' => ['/site/estanteria']],
                    ['label' => 'Mis retos', 'url' => ['/site/tusretos']],
                ],
            ]; // Enlaces para alguien con el role 2
            if (Yii::$app->user->identity->role == 2) {
                $items[] = [
                    'label' => 'Admin panel',
                    'items' => [
                        ['label' => 'Escritores', 'url' => ['/escritores']],
                        ['label' => 'Estanterías', 'url' => ['/estanterias']],
                        ['label' => 'Géneros', 'url' => ['/generos']],
                        ['label' => 'Libros', 'url' => ['/libros']],
                        ['label' => 'Retos', 'url' => ['/retos']],
                        ['label' => 'Usuarios', 'url' => ['/users']],
                    ]
                ];
            }
            $items[] = [
                'label' => 'Desconectarse (' . Yii::$app->user->identity->username . ')',
                'url' => ['/site/logout'],
                'linkOptions' => ['data-method' => 'post'],
            ];
        }

        // Agregar el botón de cambio de tema
        $items[] = '<li> <button onclick="cambiarTema()" class=btn rounded-fill">
            <span><i id="dl-icon" class="fas fa-sun"></i></span>
        </button>
    </li>';

        echo Nav::widget([
            'options' => ['class' => 'navbar-nav'],
            'items' => $items,
        ]);

        NavBar::end();
        ?>


        <main role="main" class="flex-shrink-0">
            <div class="container-fluid">
        <?=
        Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ])
        ?>
<?= Alert::widget() ?>
<?= $content ?>
            </div>
        </main>

        <footer class="footer mt-auto py-3 text-muted">
            <div class="container">
                <div class="float-left">&copy; Good Book <?= date('Y') ?>
                    <p><?= Html::a('Contactar', ['/site/contact']) ?></p></div>
                <div class="float-right">

                    <p><?= Html::a('Política de privacidad', ['site/politica']); ?></p>
                    <p><?= Html::a('Terminos y condiciones', ['site/terminos']); ?></p></div>
            </div>
        </div>
    </footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
