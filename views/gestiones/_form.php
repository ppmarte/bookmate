<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var app\models\Gestiones $model */
/** @var yii\widgets\ActiveForm $form */
?>

<div class="gestiones-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'cod_user')->textInput() ?>

    <?= $form->field($model, 'cod_estanteria')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
