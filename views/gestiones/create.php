<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Gestiones $model */

$this->title = 'Create Gestiones';
$this->params['breadcrumbs'][] = ['label' => 'Gestiones', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="gestiones-create administradores">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
