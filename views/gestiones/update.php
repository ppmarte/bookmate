<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Gestiones $model */

$this->title = 'Update Gestiones: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Gestiones', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Modificar';
?>
<div class="gestiones-update administradores">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
